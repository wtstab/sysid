\section{Examples}
\label{sec: examples}

\subsection{Mathieu oscillator}
\label{ssec: Mathieu_oscillator}

A classical periodic system is the Mathieu oscillator, which is a harmonic oscillator with a periodic stiffness. The system is described in~\cite{MSAllen_MWSracic_SChauhan_MHHansen_2011}, and the equations are
\begin{subequations}
\begin{align}
    \begin{pmatrix}
        \dot{x}   \\
        \ddot{x}
    \end{pmatrix} &= \begin{bmatrix}
         0                                        &   1                   \\
        -\omega_0^2 - \omega_1^2 \cos(\Omega t)   &   -2\zeta\omega_0
    \end{bmatrix} \begin{pmatrix}
        x        \\
        \dot{x}
    \end{pmatrix} + \begin{bmatrix}
        0     \\
        1/m
    \end{bmatrix} u
    %
    \\
    %
    y &= \begin{bmatrix}
        1   &   0
    \end{bmatrix} \begin{pmatrix}
        x        \\
        \dot{x}
    \end{pmatrix}
\end{align}
\end{subequations}
The parameters are set to: $m = 1$, $\omega_0^2 = 1$, $\omega_1^2 = 0.4$, $2\zeta\omega_0 = 0.04$. The system period is $T = 8$ s, hence $\Omega = 2\pi/T \simeq 0.78$ rad/s.


\subsubsection{Free response}
\label{sssec: Mathieu_oscillator_free}

Here we assume that the input is zero, and integrate the system from $\bm{x}_0 = (1, 0)^\transpose$ for 100 periods. The Power Spectral Density (PSD) of $y$ is shown in Fig.~\ref{fig:Mathieu_free_PSD}, with the label “Continuous”. As we can see, the system has 1 mode, and 6 harmonics are visible in its free response. 

\begin{figure}[hbtp]
\centering
\includegraphics{Mathieu/Mathieu_free_PSD.pdf}
\caption{PSD of the the free response of the Mathieu oscillator. }
\label{fig:Mathieu_free_PSD}
\end{figure}

Before applying the Periodic MOESP for free responses, $y$ is downsampled by a factor of 2. This new output is labeled “Downsampled” in Fig.~\ref{fig:Mathieu_free_PSD}. The identification with the Periodic MOESP is done by selecting 2 states, and a past window of 50. Its predicted output is also shown in Fig.~\ref{fig:Mathieu_free_PSD}, with the label “Identified”. The exact frequencies and damping ratios, the ones identified by the Periodic MOESP, and the ones identified by the peak-picking with half-power bandwidth are listed in Tab.~\ref{tab:Mathieu_free_frequency} and~\ref{tab:Mathieu_free_damping}. Since all characteristic exponents of a mode stem from the same characteristic multiplier, the error of the Periodic MOESP does not depend on the harmonic. This is due to the peaks being too low to find an intersection with the half power. Instead, the half-power bandwidth provides a good result for the principal harmonic, but its accuracy quickly degrades for the others. For this example, the results of the Periodic MOESP are also robust with respect to the downsample factor and the past window.

\begin{table}[htbp]
\caption{Exact natural frequencies of the Mathieu oscillator, and the ones identified from its free response.}
\label{tab:Mathieu_free_frequency}
\centering
\begin{tabular}{cccc}
\toprule
Harmonic    &  Exact  &  Periodic MOESP  &  Peak-picking    \\
\midrule
$-3\Omega$   &   0.217   &   0.217   &   0.217   \\
$-2\Omega$   &   0.093   &   0.093   &   0.093   \\
$-1\Omega$   &   0.032   &   0.032   &   0.032   \\
$0$          &   0.157   &   0.156   &   0.157   \\
$+1\Omega$   &   0.281   &   0.281   &   0.281   \\
$+2\Omega$   &   0.406   &   0.405   &   0.406   \\
\bottomrule
\end{tabular}
\end{table}

\begin{table}[htbp]
\caption{Exact damping ratios of the Mathieu oscillator, and the ones identified from its free response.}
\label{tab:Mathieu_free_damping}
\centering
\begin{tabular}{cccc}
\toprule
Harmonic    &  Exact  &  Periodic MOESP  &  Half-power bandwidth    \\
\midrule
$-3\Omega$   &   1.46\%   &   1.46\%   &   --        \\
$-2\Omega$   &   3.42\%   &   3.42\%   &   --        \\
$-1\Omega$   &   9.88\%   &   9.88\%   &   10.83\%   \\
$0$          &   2.03\%   &   2.03\%   &   2.12\%    \\
$+1\Omega$   &   1.13\%   &   1.13\%   &   2.00\%    \\
$+2\Omega$   &   0.78\%   &   0.78\%   &   --        \\
\bottomrule
\end{tabular}
\end{table}

\FloatBarrier

\subsubsection{Constant input}
\label{sssec: Mathieu_oscillator_constant_input}

The Mathieu oscillator is simulated again from the same initial condition, but this time using a constant input set to 0.3 N. The PSD of the response is shown in Fig.~\ref{fig:Mathieu_constant_PSD}. As expected, the constant input causes the appearance of the $n\times\mathrm{Rev}$.

\begin{figure}[hbtp]
\centering
\includegraphics{Mathieu/Mathieu_constant_PSD.pdf}
\caption{PSD of the the response of the Mathieu oscillator subject to a constant input.}
\label{fig:Mathieu_constant_PSD}
\end{figure}

The standard Periodic MOESP is applied using the same settings of the previous example. The resulting frequencies and damping ratios are listed in Tab.~\ref{tab:Mathieu_constant_frequency} and~\ref{tab:Mathieu_constant_damping}.

\begin{table}[htbp]
\caption{Exact natural frequencies of the Mathieu oscillator, and the ones identified from its response with constant input and non-zero initial conditions.}
\label{tab:Mathieu_constant_frequency}
\centering
\begin{tabular}{ccc}
\toprule
Harmonic    &  Exact  &  Periodic MOESP   \\
\midrule
$-3\Omega$   &   0.217   &  0.217         \\
$-2\Omega$   &   0.093   &  0.093         \\
$-1\Omega$   &   0.032   &  0.032         \\
$0$          &   0.157   &  0.156         \\
$+1\Omega$   &   0.281   &  0.281         \\
$+2\Omega$   &   0.406   &  0.405         \\
\bottomrule
\end{tabular}
\end{table}

\begin{table}[htbp]
\caption{Exact damping ratios of the Mathieu oscillator, and the ones identified from its response with constant input and non-zero initial conditions.}
\label{tab:Mathieu_constant_damping}
\centering
\begin{tabular}{cccc}
\toprule
Harmonic    &  Exact  &  Periodic MOESP   \\
\midrule
$-3\Omega$   &   1.46\%   &   1.46\%       \\
$-2\Omega$   &   3.42\%   &   3.42\%       \\
$-1\Omega$   &   9.88\%   &   9.88\%       \\
$0$          &   2.03\%   &   2.03\%       \\
$+1\Omega$   &   1.13\%   &   1.13\%       \\
$+2\Omega$   &   0.78\%   &   0.78\%       \\
\bottomrule
\end{tabular}
\end{table}

\FloatBarrier

\subsubsection{Process noise}
\label{sssec: Mathieu_oscillator_process_noise}

In this example we see the effect of the process noise. We have created 10 realization of a white noise with mean 0.3 and standard deviation 1, and processed it with a 4\textsuperscript{th}-order Butterworth filter. The resulting input signals are shown in Fig.~\ref{fig:Mathieu/White_noise_after_low_pass_filter_PSD}, and the output in Fig.~\ref{fig:Mathieu/Mathieu_forced_noise_PSD}.

\begin{figure}[hbtp]
\centering
\includegraphics{Mathieu/White_noise_after_low_pass_filter_PSD.pdf}
\caption{PSD of the the input for the Mathieu oscillator.}
\label{fig:Mathieu/White_noise_after_low_pass_filter_PSD}
\end{figure}

\begin{figure}[hbtp]
\centering
\includegraphics{Mathieu/Mathieu_forced_noise_PSD.pdf}
\caption{PSD of the the response of the Mathieu oscillator subject to a noisy input.}
\label{fig:Mathieu/Mathieu_forced_noise_PSD}
\end{figure}

To perform the identification we have downsampled the measures by a factor of 5, and set the past window to 10. We consider the input as composed by two parts: the measurable and the non-measurable one. The first is the mean of the input, while the second is the zero-mean noise. We then compare the result provided by the standard and the PO version of the Periodic MOESP. Fig.~\ref{fig:Mathieu/Mathieu_noise_characteristic_multipliers} shows the characteristic multipliers identified by the two algorithms, along with the exact ones. As we can see only the Periodic PO-MOESP provides unbiased estimates.

\begin{figure}[hbtp]
\centering
\includegraphics{Mathieu/Mathieu_noise_characteristic_multipliers.pdf}
\caption{Exact and identified characteristic multipliers of the Mathieu oscillator subject to a noisy input.}
\label{fig:Mathieu/Mathieu_noise_characteristic_multipliers}
\end{figure}















