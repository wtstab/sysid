# System identification

[![pipeline status](https://gitlab.windenergy.dtu.dk/wtstab/sysid/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/wtstab/sysid/-/commits/master)
[![coverage report](https://gitlab.windenergy.dtu.dk/wtstab/sysid/badges/master/coverage.svg)](https://gitlab.windenergy.dtu.dk/wtstab/sysid/-/commits/master)
[![Latest Release](https://gitlab.windenergy.dtu.dk/wtstab/sysid/-/badges/release.svg)](https://gitlab.windenergy.dtu.dk/wtstab/sysid/-/releases)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7271352.svg)](https://doi.org/10.5281/zenodo.7271352)

System identification algorithms for stability analysis of wind turbines.


## Content

Implementation of the following system identification algorithms:

- Periodic MOESP.
  - Free response.
  - Forced response without process noise.
  - Forced response with process noise.
- Partial Floquet Analysis.
- Dynamic Mode Decomposition.
- Half Power Bandwidth.

Stability analysis of discrete time linear time-periodic systems via Floquet theory.


## How to install

1.  `cd` to the folder where you cloned this repo.
2.  `pip install -e .`
3.  `cd docs`
4.  `make html`


## Documentation

The documentation is available at https://wtstab.pages.windenergy.dtu.dk/sysid/

