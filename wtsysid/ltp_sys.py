"""

Module for Linear Time-Periodic systems, and in particular Floquet theory.

"""


# %% Import.

import sys

import numpy as np
from scipy.linalg import eig, solve, norm
from scipy.integrate import solve_ivp
from scipy.fft import fft
import pandas as pd

from gmpy2 import root
from slycot import mb03vd, mb03wd


# %% Classes.


class LTPcsys:
    r"""
    Model the continuous-time Linear Time-Periodic system
    
    .. math::
        \dot{\boldsymbol{x}}(t) &= \boldsymbol{A}(t) \boldsymbol{x}(t) + \boldsymbol{B}(t) \boldsymbol{u}(t),
        \\
        \boldsymbol{y}(t)     &= \boldsymbol{C}(t) \boldsymbol{x}(t) + \boldsymbol{D}(t) \boldsymbol{u}(t),

    with:
        - :math:`t` is the continuous time.
        - :math:`\boldsymbol{x}(t)` is the state, with :math:`n_x` elements
        - :math:`\boldsymbol{u}(t)` is the input, with :math:`n_u` elements
        - :math:`\boldsymbol{y}(t)` is the output, with :math:`n_y` elements
        - All matrices are periodic with period T, i.e. :math:`\boldsymbol{A}(t+T) = \boldsymbol{A}(t)`.
    """

    def __init__(self, period, a, b=None, c=None, d=None):
        # System period.
        # By definition: a(t) = a(t+period)
        self.period = period

        # A matrix.
        self.a = a

        # Number of states.
        self.nx = a(period).shape[0]

        # Identity matrix of size nx.
        self._eye = np.identity(self.nx)

        # B matrix.
        self.b = b

        # C matrix.
        # TODO: replace with list of matrices.
        if c is None:
            self.c = lambda t: self._eye
        else:
            self.c = c

        # Number of output.
        self.ny = self.c(period).shape[0]

        # D matrix.
        self.d = d

    def simulate(
        self,
        time_end=None,
        n_points_per_period=None,
        time_start=0.0,
        t_eval=None,
        x0=None,
        u=None,
        atol=1e-12,
        rtol=1e-10,
        method="RK45",
        events=None,
        **options
    ):
        """
        Simulate the system using SciPy solve_ivp().

        Parameters
        ----------
        time_end : float, optional
            Final time. Needed only if t_eval is None
        n_points_per_period : int, optional
            Number of points in each period. Needed only if t_eval is None
        time_start : float, optional
            Initial time. Needed only if t_eval is None
        t_eval: array_like or None, optional
            Time array. If None, a time array is constructed using the
            build_time_array function.
        x0 : array_like, shape (nx,), optional
            Initial state.
        u : array_like or callable, optional
            Input. If it is a 1D array, it will be assumed constant. Otherwise
            the function will be evaluated at each time step.
        other parameters, optional
            All other parameters are passed to SciPy solve_ivp().

        Returns
        -------
        time : ndarray
            Time array
        y : (ny, len(time)) ndarray
            2D array with the output.
        """

        # Time array.
        if t_eval is None:
            # Build time array.
            time, _, _ = build_time_array(
                time_start, time_end, self.period, n_points_per_period
            )
        else:
            # Use the provided time array.
            time = t_eval

        # Set initial condition.
        if x0 is None:
            x0 = np.zeros((self.nx))

        # Function to integrate.
        # TODO: add option to reduce the time to [0 period].
        if u is None:
            # Free response.
            def odefun(t, x):
                x = self.a(t) @ x.reshape(-1, 1)
                return x.ravel()

        elif type(u) is np.ndarray:
            # Constant input.
            u = u.reshape(-1, 1)

            def odefun(t, x):
                x = self.a(t) @ x.reshape(-1, 1) + self.b(t) @ u
                return x.ravel()

        else:
            # Time-varying input.
            def odefun(t, x):
                x = self.a(t) @ x.reshape(-1, 1) + self.b(t) @ u(t)
                return x.ravel()

        # Time integration.
        sol = solve_ivp(
            fun=odefun,
            t_span=(time[0], time[-1]),
            y0=x0,
            method=method,
            t_eval=time,
            events=events,
            atol=atol,
            rtol=rtol,
            vectorized=False,
            **options
        )

        # Output.
        y = np.zeros((self.ny, time.size))
        # TODO: add option to reduce the time to [0 period].
        if u is None or self.d is None:
            # Free response or no D(t) matrix.
            for k in range(time.size):
                t = time[k]
                y[:, k] = (self.c(t) @ sol.y[:, [k]]).ravel()

        elif type(u) is np.ndarray:
            # Constant input.
            for k in range(time.size):
                t = time[k]
                y[:, k] = (self.c(t) @ sol.y[:, [k]] + self.d(t) @ u).ravel()
        else:
            # Time-varying input.
            for k in range(time.size):
                t = time[k]
                y[:, k] = (self.c(t) @ sol.y[:, [k]] + self.d(t) @ u(t)).ravel()

        # Return.
        return time, y

    def transition_matrix(
        self,
        n_points_per_period,
        time_start=0.0,
        time_end=None,
        atol=1e-12,
        rtol=1e-10,
        method="RK45",
        events=None,
        **options
    ):
        """
        Compute the state transition matrix.

        Parameters
        ----------
        n_points_per_period : int
            Number of points in each period.
        time_start : float, optional
            Initial time.
        time_end : float, optional
            Final time.
        other parameters, optional
            All other parameters are passed to SciPy solve_ivp().

        Returns
        -------
        time : ndarray
            Time array
        tm : (nx, nx, len(time)) ndarray
            3D array with the transition matrix.
        """

        # Time array.
        if time_end is None:
            time_end = self.period
            time = np.linspace(0.0, self.period, n_points_per_period)
        else:
            time, _, _ = build_time_array(
                time_start, time_end, self.period, n_points_per_period
            )

        # Set initial condition.
        tm0 = self._eye.reshape(-1)

        # Function to integrate.
        def odefun(t, tm):
            return np.reshape(self.a(t) @ tm.reshape(self.nx, self.nx), -1)

        # Time integration.
        sol = solve_ivp(
            fun=odefun,
            t_span=(time[0], time[-1]),
            y0=tm0,
            method=method,
            t_eval=time,
            events=events,
            atol=atol,
            rtol=rtol,
            vectorized=False,
            **options
        )

        # Reshape the transition matrix.
        tm = sol.y.reshape(self.nx, self.nx, -1)

        return sol.t, tm

    def c2d(self, n_points_per_period):
        """
        Continuous to discrete time conversion.
        """

        # Transition matrix over 1 period.
        time_tm, tm = self.transition_matrix(n_points_per_period)
        dt = np.mean(np.diff(time_tm))

        # Discrete time A[t] matrix.
        ad = np.zeros((self.nx, self.nx, n_points_per_period - 1))
        for t in range(ad.shape[2]):
            ad[:, :, t] = np.transpose(solve(tm[:, :, t].T, tm[:, :, t + 1].T))

        # Form the new system.
        sysd = LTPdsys(ad, dt=dt)
        return sysd


class LTPdsys:
    r"""
    Model the discrete-time Linear Time-Periodic system
    
    .. math::
        \boldsymbol{x}(t + 1) &= \boldsymbol{A}(t) \boldsymbol{x}(t) + \boldsymbol{B}(t) \boldsymbol{u}(t),
        \\
        \boldsymbol{y}(t)     &= \boldsymbol{C}(t) \boldsymbol{x}(t) + \boldsymbol{D}(t) \boldsymbol{u}(t),

    with:
        - :math:`t` is the discrete time.
        - :math:`\boldsymbol{x}(t)` is the state, with :math:`n_x` elements
        - :math:`\boldsymbol{u}(t)` is the input, with :math:`n_u` elements
        - :math:`\boldsymbol{y}(t)` is the output, with :math:`n_y` elements
        - All matrices are periodic with period T, i.e. :math:`\boldsymbol{A}(t+T) = \boldsymbol{A}(t)`.
    """

    def __init__(self, a, b=None, c=None, d=None, dt=1):
        # A matrix.
        self.a = a

        # System period.
        # By definition: a[:, :, t] = a[:, :, t+period]
        self.period = self.a.shape[2] + 1

        # Number of states.
        self.nx = a.shape[0]

        # Identity matrix of size nx.
        self._eye = np.identity(self.nx)

        # B matrix.
        self.b = b

        # C matrix.
        # TODO: replace with list of matrices.
        if c is None:
            self.c = np.broadcast_to(
                self._eye[:, :, np.newaxis], (self.nx, self.nx, self.a.shape[2])
            )
        else:
            self.c = c

        # Number of output.
        self.ny = self.c.shape[0]

        # D matrix.
        self.d = d

        # Time step for discrete to continuous time conversion.
        self.dt = dt

    def simulate(self, time_end, x0=None, u=None):
        """
        Simulate the system.
        """

        # Preallocate the output.
        y = np.zeros((self.ny, time_end))

        # Set initial condition.
        if x0 is None:
            x = np.zeros((self.nx, 1))
        else:
            x = x0.reshape(-1, 1)

        if u is None or (self.b is None and self.d is None):
            # Time loop.
            for t in range(time_end):
                # Reduce t to the interval [0, period-1]
                tr = t % self.a.shape[2]
                # Update output
                y[:, t] = (self.c[:, :, tr] @ x).ravel()
                # Update state
                x = self.a[:, :, tr] @ x

        else:  # u is not None
            if self.b is not None and self.d is None:
                for t in range(time_end):
                    tr = t % self.a.shape[2]
                    y[:, t] = (self.c[:, :, tr] @ x).ravel()
                    x = self.a[:, :, tr] @ x + self.b[:, :, tr] @ u[:, [t]]

            elif self.b is None and self.d is not None:
                for t in range(time_end):
                    tr = t % self.a.shape[2]
                    y[:, t] = (
                        self.c[:, :, tr] @ x + self.d[:, :, tr] @ u[:, [t]]
                    ).ravel()
                    x = self.a[:, :, tr] @ x

            else:  # self.b is not None and self.d is not None
                for t in range(time_end):
                    tr = t % self.a.shape[2]
                    y[:, t] = (
                        self.c[:, :, tr] @ x + self.d[:, :, tr] @ u[:, [t]]
                    ).ravel()
                    x = self.a[:, :, tr] @ x + self.b[:, :, tr] @ u[:, [t]]

        return y

    def transition_matrix(self, time_start, time_end):
        """
        Compute the state transition matrix.

        Parameters
        ----------
        time_start : int
            Initial time tag.
        time_end : int
            Final time tag.

        Returns
        -------
        tm : (nx, nx, time_end - time_start) ndarray
            3D array with the transition matrix.
        """
        # Preallocate.
        tm = np.zeros((self.nx, self.nx, time_end - time_start))

        # Set initial condition.
        tm[:, :, 0] = self._eye
        for t in range(1, time_end - time_start):
            # Reduce t to the interval [0, period-1]
            tr = t % self.a.shape[2]
            # Evaluate the recursion.
            tm[:, :, t] = self.a[:, :, tr - 1] @ tm[:, :, t - 1]

        return tm

    def observability_matrix(self, time_start, time_end):
        """
        Computes the observability matrix.

        Parameters
        ----------
        time_start : int
            Initial time tag.
        time_end : int
            Final time tag relative to time_start. The observability matrix is
            computed between time_start and time_start + time_end - 1.

        Returns
        -------
        obs : float ndarray
            Observability matrix.
        """

        # Preallocate.
        obs = np.zeros((self.ny * time_end, self.nx))

        # Early exit.
        if time_end == 0:
            return

        # Initialization of the state transition matrix.
        tm = self._eye

        # Loop over the blocks.
        obs[: self.ny, :] = self.c[:, :, time_start % self.a.shape[2]]
        for t in range(1, time_end):
            # Reduce t to the interval [0, period-1]
            tr = (time_start + t) % self.a.shape[2]
            # Update the transition matrix.
            tm = self.a[:, :, tr - 1] @ tm
            # Block t of the observability matrix.
            obs[t * self.ny : (t + 1) * self.ny, :] = self.c[:, :, tr] @ tm

        # Return.
        return obs

    def floquet(self, p0=None, method="product"):
        """
        Perform the stability analysis using Floquet's theory.

        Parameters
        ----------
        p0 : ndarray
            Initial value of the periodic transformation used to convert the
            system into a time-invariant one. Default to the identity matrix.

        method : str
            Method used to compute the characteristic multipliers. Can be:
                - `"product"`: the monodromy matrix is obtained by explicitly computing the matrix product. This method is not backward stable.
                - `"schur_slycot"`: use the periodic Schur decomposition from SLYCOT. This method is backward stable.

        Returns
        -------
        Several variables are stored in self.
        """
        # Initial value of the periodic transformation.
        if p0 is None:
            p0 = np.identity(self.nx)

        # Compute the transition matrix over one period.
        self.tm = self.transition_matrix(time_start=0, time_end=self.period)

        # Get the monodromy matrix.
        self.monodromy_matrix = self.tm[:, :, -1]

        # Characteristic multipliers.
        # There will be self.nx characteristic multipliers.
        if method == "product":
            # Compute the characteristic multipliers as the eigenvalues of the matrix product.
            self.characteristic_multipliers, monodromy_matrix_eigenvectors = eig(
                self.monodromy_matrix
            )

        elif method == "schur_slycot":
            # Set up parameters of Fortran subroutines.
            ilo = iloz = 1
            ihi = ihiz = self.nx
            job = "E"  # Compute only the eigenvalues.
            compz = "N"  # Do not accumulate the transformation matrices.
            # Transform the A matrix in periodic Hessenberg form.
            hq, _ = mb03vd(self.nx, ilo, ihi, self.a)
            # Apply the periodic Schur decomposition.
            _, _, self.characteristic_multipliers = mb03wd(
                job, compz, self.nx, ilo, ihi, iloz, ihiz, hq, np.empty_like(self.a)
            )
            # For now we still compute the eigenvectors from the matrix product. Later we will have to use the periodic Schur form.
            _, monodromy_matrix_eigenvectors = eig(self.monodromy_matrix)

        else:
            raise ValueError('method must be "product" or "schur_slycot".')

        modulus_theta = np.abs(self.characteristic_multipliers)
        phase_theta = np.angle(self.characteristic_multipliers)  # [rad]

        # Define the shifts.
        # Put the 0 shift in the middle of the array.
        shift = np.arange(0, self.period)
        shift0 = self.period // 2
        shift = np.roll(shift, shift0)

        # Discrete time characteristic exponents.
        # The characteristic exponents are stored in a 2D array.
        #  - row index spans the harmonics, and has length self.period.
        #  - column index spans the modes, and has length self.nx.
        try:
            # This requires the gmpy2 package.
            modulus_theta_root = np.zeros_like(modulus_theta)
            for i in range(self.nx):
                modulus_theta_root[i] = float(root(modulus_theta[i], self.period))

        except NameError:
            # Resort to numpy.
            modulus_theta_root = np.power(modulus_theta, 1.0 / self.period)

        self.characteristic_exponents = modulus_theta_root.reshape(1, -1) * np.exp(
            1j
            * (phase_theta.reshape(1, -1) + (2 * np.pi * shift).reshape(-1, 1))
            / self.period
        )

        # Discrete to continuous time conversion.
        self.characteristic_exponents_continuous = (
            np.log(self.characteristic_exponents) / self.dt
        )

        # Frequencies and damping ratios.
        abs_eta = np.abs(self.characteristic_exponents_continuous)  # [rad/s]
        self.natural_frequencies = abs_eta / (2 * np.pi)  # [Hz]
        self.damped_frequencies = np.abs(
            np.imag(self.characteristic_exponents_continuous)
        )
        self.damping_ratios = (
            -np.real(self.characteristic_exponents_continuous) / abs_eta
        )  # [-]

        # Floquet factor.
        floquet_factor_eigenvectors = solve(p0, monodromy_matrix_eigenvectors)
        self.floquet_factor = solve(
            floquet_factor_eigenvectors,
            floquet_factor_eigenvectors @ np.diag(self.characteristic_exponents[shift0, :]),  # fmt: skip
        )

        # Periodic matrix to transform the A matrix into the Floquet factor.
        self.p = np.zeros((self.nx, self.nx, self.period - 1), dtype=np.complex128)
        self.p[:, :, 0] = p0
        for t in range(1, self.p.shape[2]):
            self.p[:, :, t] = np.transpose(
                solve(
                    floquet_factor_eigenvectors.T,
                    np.transpose(
                        self.tm[:, :, t] @ monodromy_matrix_eigenvectors @ np.diag(self.characteristic_exponents[shift0, :] ** (0 - t))  # fmt: skip
                    ),
                )
            )

        # Evaluate a term needed for the observed mode shapes.
        xi = np.zeros((self.ny, self.nx, self.period - 1), dtype=np.complex128)
        for t in range(xi.shape[2]):
            xi[:, :, t] = (
                self.c[:, :, t] @ self.p[:, :, t] @ floquet_factor_eigenvectors
            )

        # Observed Mode shapes.
        # The mode shapes are stored in a 3D array.
        #  - index 0 spans the harmonics, and has length self.period.
        #  - index 1 spans the modes, and has length self.nx.
        #  - index 2 spans the output, and has length self.ny.
        self.mode_shapes = np.transpose(
            np.roll(fft(xi) / (self.period - 1), shift0, axis=2), axes=[2, 1, 0]
        )

        # Participation factors.
        # The participation factors are stored in a 2D array.
        #  - row index spans the harmonics, and has length self.period.
        #  - column index spans the modes, and has length self.nx.
        mode_shapes_norm = norm(self.mode_shapes, axis=2)
        self.participation_factors = mode_shapes_norm / np.sum(
            mode_shapes_norm, axis=0, keepdims=True
        )

        # Output-specific participation factors, i.e. single-output.
        # The output-specific participation factors are stored in a 3D array.
        #  - index 0 spans the harmonics, and has length self.period.
        #  - index 1 spans the modes, and has length self.nx.
        #  - index 2 spans the output, and has length self.ny.
        mode_shapes_abs = np.abs(self.mode_shapes)
        self.participation_factors_single_output = mode_shapes_abs / np.sum(
            mode_shapes_abs, axis=0, keepdims=True
        )

    def print_floquet_results(self, nh=5, file=sys.stdout, method="sort"):
        r"""
        Print and return the Floquet harmonics with the greatest participation factors.

        Parameters
        ----------
        nh : int
            Number of harmonics to return / print.
        file : file identifier.
            By default it prints to standard output. If None, no print is done.
        method : str
            How to collect the harmonics. Possible choices are:
                - "sort": Collect the harmonics from `-nh` to `+nh`, with the one with the maximum participation in the center.
                - "max": Take the `nh`harmonics with the largest participation.

        Returns
        -------
        res : list of DataFrame
            List of pandas DataFrame, containing a subset of the results of Floquet analysis.
        """
        # Determine the indices of the harmonics.
        if method == "sort":
            # Look for maximum participation for all modes.
            ind_0 = np.argmax(self.participation_factors, axis=0)
            # The names are -nh, ..., 0, +nh.
            ind_names = np.arange(-nh, +nh + 1)
            # Get the harmonics.
            ind = ind_0[np.newaxis, :] + ind_names[:, np.newaxis]

            ind_names = np.broadcast_to(ind_names[:, np.newaxis], (2 * nh + 1, self.nx))

        elif method == "max":
            # Look for the nh largest participation factors.
            ind = np.argpartition(self.participation_factors, -nh, axis=0)[-nh:, :]
            ind_names = ind - np.argmax(self.participation_factors, axis=0)

        else:
            raise ValueError("method must be 'sort' or 'max'.")

        # Take the stability results.
        ix = np.arange(self.nx)[np.newaxis, :]
        natural_frequencies = self.natural_frequencies[ind, ix]
        damping_ratios = self.damping_ratios[ind, ix] * 100.0
        participation_factors = self.participation_factors[ind, ix]

        # Put them in DataFrames.
        res = []
        for i in range(self.nx):
            res.append(
                pd.DataFrame(
                    data=np.column_stack(
                        (
                            natural_frequencies[:, i],
                            damping_ratios[:, i],
                            participation_factors[:, i],
                        )
                    ),
                    columns=(
                        "Natural frequency [Hz]",
                        "Damping ratio [%]",
                        "Participation [-]",
                    ),
                    index=pd.Index(ind_names[:, i], name="Harmonic"),
                )
            )

        if file is not None:
            for i in range(self.nx):
                print("Mode {}".format(i), file=file)
                print(res[i], file=file)
                print("\n", file=file)

        return res


# %% Helper functions.


def build_time_array(time_start, time_end, period, n_points_per_period):
    r"""
    Build the time array with an integer number of points per period.

    Parameters
    ----------
    time_start : float
        Initial time [s].
    time_end : float
        Final time [s].
    period : float
        Period of the system [s].
    n_points_per_period: int
        Number of points in each period.

    Returns
    -------
    time : ndarray
        Time array [s].
    dt : float
        Time step [s].
    dpsi : float
        Azimuth step [deg].
    """

    # Time between time_start and time_end.
    duration = time_end - time_start

    # Number of periods.
    # n_periods = int(duration // period)

    # Time step.
    dt = period / n_points_per_period

    # Number of time steps.
    n_dt = int(duration // dt)

    # Time array.
    time = time_start + dt * np.arange(n_dt + 1)

    # Azimuth step.
    # Omega = 2 * pi / period;   # Angular speed [rad/s].
    # dpsi = Omega * dt * 180 / pi
    dpsi = 2.0 / period * dt * 180.0  # [deg]

    # Return.
    return time, dt, dpsi
