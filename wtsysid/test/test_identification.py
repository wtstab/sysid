"""
Module for testing identification.
"""


# %% Import.

import pytest
import numpy as np
from scipy import signal
from numpy.testing import assert_array_equal, assert_allclose

from wtsysid.identification import _t_data_mat, pmoesp, pfa


# %% Test identification algorithms on the Mathieu oscillator.


class TestMathieuFree:
    @staticmethod
    @pytest.fixture(scope="class")
    def simulate_mathieu_free():
        from wtsysid.examples.mathieu import Mathieu

        mathieu_c = Mathieu()

        # Initial condition.
        x0 = np.array([1.0, 0.0])

        # Simulate 100 periods.
        time_end = mathieu_c.period * 100
        n_points_per_period = 400

        # Simulation.
        time, y = mathieu_c.simulate(time_end, n_points_per_period, x0=x0)

        # Stability.
        mathieu_d = mathieu_c.c2d(n_points_per_period)
        mathieu_d.floquet(method="product")

        return time, y, mathieu_d

    @staticmethod
    def test_pmoesp_free(simulate_mathieu_free):
        # Get data.
        time, y, mathieu_d = simulate_mathieu_free

        # Downsample the simulated output.
        downsample_step = 2

        # Digital anti-aliasing filter.
        sos = signal.butter(
            N=2, Wn=1.0 / downsample_step, btype="low", analog=False, output="sos"
        )

        # Apply the filter.
        y_filtered = signal.sosfiltfilt(sos, y)

        # Decimate.
        time_ident = time[0:-1:downsample_step]
        dt_ident = np.mean(np.diff(time_ident))
        y_ident = y_filtered[:, 0:-1:downsample_step]
        n_points_per_period = mathieu_d.a.shape[2] + 1
        period_ident = int(np.round(n_points_per_period / downsample_step))

        # Identify.
        past_window = 50

        sys_ident, x0_ident, singular_values = pmoesp(
            y_ident, nx=2, period=period_ident, past_window=past_window
        )
        sys_ident.dt = dt_ident

        # Stability.
        sys_ident.floquet(method="product")

        # Gather stability results.
        res_exact = mathieu_d.print_floquet_results(file=None)
        res_ident = sys_ident.print_floquet_results(file=None)

        # Look for maximum participation.
        i_mode = 0
        i_max_participation_exact = (
            res_exact[i_mode].loc[:, "Participation [-]"].argmax()
        )
        i_max_participation_ident = (
            res_ident[i_mode].loc[:, "Participation [-]"].argmax()
        )

        # Check frequency.
        assert_allclose(
            res_exact[i_mode]
            .iloc[i_max_participation_exact, :]
            .loc["Natural frequency [Hz]"],
            res_ident[i_mode]
            .iloc[i_max_participation_ident, :]
            .loc["Natural frequency [Hz]"],
            atol=5e-3,
        )

        # Check damping ratio.
        assert_allclose(
            res_exact[i_mode]
            .iloc[i_max_participation_exact, :]
            .loc["Damping ratio [%]"],
            res_ident[i_mode]
            .iloc[i_max_participation_ident, :]
            .loc["Damping ratio [%]"],
            atol=1e-4,
        )

    @staticmethod
    def test_pfa(simulate_mathieu_free):
        # Get data.
        time, y, mathieu_d = simulate_mathieu_free

        # Downsample the simulated output.
        downsample_step = 2

        # Digital anti-aliasing filter.
        sos = signal.butter(
            N=2, Wn=1.0 / downsample_step, btype="low", analog=False, output="sos"
        )

        # Apply the filter.
        y_filtered = signal.sosfiltfilt(sos, y)

        # Decimate.
        time_ident = time[0:-1:downsample_step]
        dt_ident = np.mean(np.diff(time_ident))
        y_ident = y_filtered[:, 0:-1:downsample_step]
        n_points_per_period = mathieu_d.a.shape[2] + 1
        period_ident = int(np.round(n_points_per_period / downsample_step))

        # Identify.
        past_window = 50

        natural_frequencies, damping_ratios, _, mode_shapes = pfa(
            y_ident,
            nx=2,
            period=period_ident,
            past_window=past_window,
            dt=dt_ident,
            compute_mode_shapes=True,
        )
        # There is only 1 output.
        mode_shapes = mode_shapes[:, :, 0]

        # Gather stability results.
        res_exact = mathieu_d.print_floquet_results(file=None)

        # Look for maximum participation.
        i_mode = 0
        i_max_participation_exact = (
            res_exact[i_mode].loc[:, "Participation [-]"].argmax()
        )
        mode_shapes_mag = np.abs(mode_shapes)
        i_max_participation_ident = mode_shapes_mag[:, i_mode].argmax()

        # Check frequency.
        assert_allclose(
            res_exact[i_mode]
            .iloc[i_max_participation_exact, :]
            .loc["Natural frequency [Hz]"],
            natural_frequencies[i_max_participation_ident, i_mode],
            atol=5e-4,
        )

        # Check damping ratio.
        assert_allclose(
            res_exact[i_mode]
            .iloc[i_max_participation_exact, :]
            .loc["Damping ratio [%]"],
            damping_ratios[i_max_participation_ident, i_mode] * 100.0,
            atol=5e-6,
        )


class TestMathieuSteadyInput:
    @staticmethod
    @pytest.fixture(scope="class")
    def simulate_mathieu_steady_input():
        from wtsysid.examples.mathieu import Mathieu

        mathieu_c = Mathieu()

        # Initial condition.
        x0 = np.array([1.0, 0.0])

        # Input.
        u = np.array([0.3])

        # Simulate 100 periods.
        time_end = mathieu_c.period * 100
        n_points_per_period = 400

        # Simulation.
        time, y = mathieu_c.simulate(time_end, n_points_per_period, x0=x0, u=u)

        # Stability.
        mathieu_d = mathieu_c.c2d(n_points_per_period)
        mathieu_d.floquet(method="product")

        return time, y, u, mathieu_d

    @staticmethod
    def test_pmoesp(simulate_mathieu_steady_input):
        # Get data.
        time, y, u, mathieu_d = simulate_mathieu_steady_input

        # Downsample the simulated output.
        downsample_step = 2

        # Digital anti-aliasing filter.
        sos = signal.butter(
            N=2, Wn=1.0 / downsample_step, btype="low", analog=False, output="sos"
        )

        # Apply the filter.
        y_filtered = signal.sosfiltfilt(sos, y)

        # Decimate.
        time_ident = time[0:-1:downsample_step]
        dt_ident = np.mean(np.diff(time_ident))
        y_ident = y_filtered[:, 0:-1:downsample_step]
        n_points_per_period = mathieu_d.a.shape[2] + 1
        period_ident = int(np.round(n_points_per_period / downsample_step))

        # Create the input.
        u_ident = np.broadcast_to(u, (1, time_ident.size))

        # Identify.
        past_window = 50

        sys_ident, x0_ident, singular_values = pmoesp(
            y_ident,
            nx=2,
            period=period_ident,
            past_window=past_window,
            u=u_ident,
            zero_mean_input=False,
        )
        sys_ident.dt = dt_ident

        # Stability.
        sys_ident.floquet(method="product")

        # Gather stability results.
        res_exact = mathieu_d.print_floquet_results(file=None)
        res_ident = sys_ident.print_floquet_results(file=None)

        # Look for maximum participation.
        i_mode = 0
        i_max_participation_exact = (
            res_exact[i_mode].loc[:, "Participation [-]"].argmax()
        )
        i_max_participation_ident = (
            res_ident[i_mode].loc[:, "Participation [-]"].argmax()
        )

        # Check frequency.
        assert_allclose(
            res_exact[i_mode]
            .iloc[i_max_participation_exact, :]
            .loc["Natural frequency [Hz]"],
            res_ident[i_mode]
            .iloc[i_max_participation_ident, :]
            .loc["Natural frequency [Hz]"],
            atol=5e-2,
        )

        # Check damping ratio.
        assert_allclose(
            res_exact[i_mode]
            .iloc[i_max_participation_exact, :]
            .loc["Damping ratio [%]"],
            res_ident[i_mode]
            .iloc[i_max_participation_ident, :]
            .loc["Damping ratio [%]"],
            atol=5e-3,
        )

    @staticmethod
    def test_pfa(simulate_mathieu_steady_input):
        # Get data.
        time, y, _, mathieu_d = simulate_mathieu_steady_input

        # Downsample the simulated output.
        downsample_step = 2

        # Digital anti-aliasing filter.
        sos = signal.butter(
            N=2, Wn=1.0 / downsample_step, btype="low", analog=False, output="sos"
        )

        # Apply the filter.
        y_filtered = signal.sosfiltfilt(sos, y)

        # Decimate.
        time_ident = time[0:-1:downsample_step]
        dt_ident = np.mean(np.diff(time_ident))
        y_ident = y_filtered[:, 0:-1:downsample_step]
        n_points_per_period = mathieu_d.a.shape[2] + 1
        period_ident = int(np.round(n_points_per_period / downsample_step))

        # Identify.
        past_window = 50

        natural_frequencies, damping_ratios, _, mode_shapes = pfa(
            y_ident,
            nx=3,
            period=period_ident,
            past_window=past_window,
            dt=dt_ident,
            compute_mode_shapes=True,
        )
        # There is only 1 output.
        mode_shapes = mode_shapes[:, :, 0]

        # Gather stability results.
        res_exact = mathieu_d.print_floquet_results(file=None)

        # Look for maximum participation.
        i_mode_exact = 0
        i_max_participation_exact = (
            res_exact[i_mode_exact].loc[:, "Participation [-]"].argmax()
        )
        i_mode_dent = 1
        mode_shapes_mag = np.abs(mode_shapes)
        i_max_participation_ident = mode_shapes_mag[:, i_mode_dent].argmax()

        # Check frequency.
        assert_allclose(
            res_exact[i_mode_exact]
            .iloc[i_max_participation_exact, :]
            .loc["Natural frequency [Hz]"],
            natural_frequencies[i_max_participation_ident, i_mode_dent],
            atol=5e-4,
        )

        # Check damping ratio.
        assert_allclose(
            res_exact[i_mode_exact]
            .iloc[i_max_participation_exact, :]
            .loc["Damping ratio [%]"],
            damping_ratios[i_max_participation_ident, i_mode_dent] * 100.0,
            atol=5e-6,
        )


# %% Other test.


def test_t_data_mat():
    # Parameters.
    t = 3
    i = 2
    d = 8
    period = 13
    n_periods = 4

    # Create the data matrix.
    mat = _t_data_mat(t, i, d, period, n_periods)

    # Check it.
    assert_array_equal(mat.shape, (d, n_periods))
    assert mat[0, 0] == t + i * period
    assert mat[-1, -1] == t + (i + n_periods - 1) * period + d - 1
