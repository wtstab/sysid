# %% Import.

import numpy as np
from numpy.testing import assert_allclose

from wtsysid.utils import get_coleman_rotating_to_fixed, apply_coleman_rotating_to_fixed


# %% Test.


def test_coleman_3_blades():
    # Number of blades.
    n_blades = 3

    # Time array.
    time = np.linspace(0.0, 100.0, 11)

    # Rotor speed in [rad/s].
    rotor_speed = 2.0

    # Array of azimuth angles.
    psi = (
        rotor_speed * time.reshape(-1, 1)
        + np.arange(n_blades).reshape(1, -1) * 2 * np.pi / n_blades
    )

    # Get the Coleman transformation.
    col = get_coleman_rotating_to_fixed(psi)

    # Explicitly write the Coleman transformation.
    col2 = np.zeros((time.shape[0], 3, 3))
    # fmt: off
    for i in range(time.size):
        col2[i, :, :] = np.array([[1,                       1,                       1                      ],
                                  [2.0 * np.cos(psi[i, 0]), 2.0 * np.cos(psi[i, 1]), 2.0 * np.cos(psi[i, 2])],
                                  [2.0 * np.sin(psi[i, 0]), 2.0 * np.sin(psi[i, 1]), 2.0 * np.sin(psi[i, 2])]]) / 3.0
    # fmt: on
    # Check.
    assert_allclose(col, col2, atol=1e-10)


def test_coleman_2_blades():
    # Number of blades.
    n_blades = 2

    # Time array.
    time = np.linspace(0.0, 100.0, 11)

    # Rotor speed in [rad/s].
    rotor_speed = 2.0

    # Array of azimuth angles.
    psi = (
        rotor_speed * time.reshape(-1, 1)
        + np.arange(n_blades).reshape(1, -1) * 2 * np.pi / n_blades
    )

    # Get the Coleman transformation.
    col = get_coleman_rotating_to_fixed(psi)

    # Explicitly write the Coleman transformation.
    col2 = np.full((time.shape[0], 2, 2), 0.5)
    col2[:, 1, 0] = -0.5

    # Check.
    assert_allclose(col, col2, atol=1e-10)


def test_coleman_transformation():
    # Number of blades.
    n_blades = 3

    # Time array [s].
    time = np.linspace(0.0, 30.0, 101)

    # Rotor speed in [rad/s].
    rotor_speed = 5 * np.pi / 30

    # Array of azimuth angles.
    psi = (
        rotor_speed * time.reshape(-1, 1)
        + np.arange(n_blades).reshape(1, -1) * 2 * np.pi / n_blades
    )

    # Create periodic output.
    y = np.zeros((time.size, 3, 7))
    for i in range(y.shape[2]):
        for b in range(y.shape[1]):
            y[:, b, i] = 2.0 + (i + 0.5) * np.cos(psi[:, b])

    # Apply the Coleman transformation.
    y_mbc = apply_coleman_rotating_to_fixed(psi, y)

    # Check the result by explicit loop.
    y_mbc2 = np.zeros(y.shape)
    col = get_coleman_rotating_to_fixed(psi)
    for c in range(y.shape[2]):
        for t in range(y.shape[0]):
            y_mbc2[t, :, c] = col[t, :, :] @ y[t, :, c]

    assert_allclose(y_mbc, y_mbc2, atol=1e-10)

    # Check mean value.
    assert_allclose(np.mean(y_mbc[:, 0, 0]), 2.0, atol=1e-10)

    # Check sin.
    assert_allclose(np.mean(y_mbc[:, 2, 0]), 0.0, atol=1e-10)

    # Plot.
    if False:
        import matplotlib.pyplot as plt

        # Select channel.
        c = 0

        fig, ax = plt.subplots(num="Blade coordinates")
        ax.set_xlabel("Time [s]")
        ax.set_ylabel("Output [N]")
        ax.grid()
        for b in range(y.shape[1]):
            ax.plot(time, y[:, b, c], label="Blade {}".format(b + 1))
        ax.legend()

        names_mbc = ["Average", "Cos", "Sin"]
        fig, ax = plt.subplots(num="Multi-Blade coordinates")
        ax.set_xlabel("Time [s]")
        ax.set_ylabel("Output [N]")
        ax.grid()
        for m in range(y_mbc.shape[1]):
            ax.plot(time, y_mbc[:, m, c], label=names_mbc[m])
        ax.legend()
