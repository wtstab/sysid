"""
Module for testing floquet.
"""


# %%

import numpy as np
from numpy.random import default_rng
from numpy.testing import assert_allclose, assert_array_equal

from gmpy2 import root

from wtsysid.ltp_sys import LTPdsys


# %% All test.


def test_sim():
    nx = 2
    length = 5

    rg = default_rng(0)
    a = rg.random((nx, nx, length))

    sys = LTPdsys(a)

    x0 = rg.random((nx))

    sys.simulate(time_end=16, x0=x0)


def test_transition_matrix_1():
    """
    Test the transition matrix.
    """

    # Setup system.
    nx = 2
    length = 10

    rg = default_rng(0)
    a = rg.random((nx, nx, length))

    sys = LTPdsys(a)

    # Select initial condition.
    x0 = rg.random((nx))

    # Simulate the free response.
    y1 = sys.simulate(time_end=30, x0=x0)

    # Compute the transition matrix.
    tm = sys.transition_matrix(0, 30)

    # Use the transition matrix to get the free response.
    y2 = np.zeros_like(y1)
    for t in range(y2.shape[1]):
        y2[:, t] = tm[:, :, t] @ x0

    # Compare.
    assert_allclose(y1, y2, atol=1e-10)


def test_floquet_1():
    """
    Test the Floquet stability analysis.
    """

    # Setup system.
    nx = 4
    ny = 7
    length = 10
    dt = 0.01

    rg = default_rng(0)
    a = rg.random((nx, nx, length))
    c = rg.random((ny, nx, length))

    sys = LTPdsys(a, c=c, dt=dt)

    # Do Floquet stability analysis.
    sys.floquet()

    # Check shape.
    assert_array_equal(sys.characteristic_exponents.shape, [sys.period, sys.nx])


def test_floquet_factor():
    pass

    # To be implemented with good system.
    # Test:
    # for any k in [1 dPeriod]
    #   P(:,:,k+1)\A(:,:,k)*P(:,:,k)
    # must equate R.


# @dec.slow
def test_root():
    """
    Test gmpy2 root() against numpy power.
    """

    # Select characteristic multiplier from extremely stable to very unstable.
    abs_theta = np.linspace(0, 2, 1001)
    # Select periods from time invariant to periodic with high Nyquist frequency.
    periods = np.arange(1, 1001)

    # Compute the roots using numpy.
    root_1 = np.power(abs_theta.reshape(1, -1), 1 / periods.reshape(-1, 1))

    # Compute the roots using gmpy2.
    root_2 = np.zeros_like(root_1)
    for i in range(periods.size):
        for j in range(abs_theta.size):
            root_2[i, j] = float(root(abs_theta[j], int(periods[i])))

    # Compare.
    assert_allclose(root_1, root_2, atol=1e-10)


def test_observability_matrix():
    # Setup system.
    nx = 2
    ny = 3
    length = 4

    rg = default_rng(0)
    a = rg.random((nx, nx, length))
    c = rg.random((ny, nx, length))

    sys = LTPdsys(a, c=c)

    # Observability matrix computed using for loop.
    obs_1 = sys.observability_matrix(time_start=2, time_end=7)

    # Correct observability matrix.
    # fmt: off
#    obs_2 = np.block([
#            [c[:, :, 2]],
#            [c[:, :, 3] @ a[:, :, 2]],
#            [c[:, :, 4] @ a[:, :, 3] @ a[:, :, 2]],
#            [c[:, :, 5] @ a[:, :, 4] @ a[:, :, 3] @ a[:, :, 2]],
#            [c[:, :, 6] @ a[:, :, 5] @ a[:, :, 4] @ a[:, :, 3] @ a[:, :, 2]],
#            [c[:, :, 7] @ a[:, :, 6] @ a[:, :, 5] @ a[:, :, 4] @ a[:, :, 3] @ a[:, :, 2]],
#            [c[:, :, 8] @ a[:, :, 7] @ a[:, :, 6] @ a[:, :, 5] @ a[:, :, 4] @ a[:, :, 3] @ a[:, :, 2]],
#            ])
    # Correct observability matrix with time indices reduced to fit in the period.
    obs_2 = np.block([
            [c[:, :, 2]],
            [c[:, :, 3] @ a[:, :, 2]],
            [c[:, :, 0] @ a[:, :, 3] @ a[:, :, 2]],
            [c[:, :, 1] @ a[:, :, 0] @ a[:, :, 3] @ a[:, :, 2]],
            [c[:, :, 2] @ a[:, :, 1] @ a[:, :, 0] @ a[:, :, 3] @ a[:, :, 2]],
            [c[:, :, 3] @ a[:, :, 2] @ a[:, :, 1] @ a[:, :, 0] @ a[:, :, 3] @ a[:, :, 2]],
            [c[:, :, 0] @ a[:, :, 3] @ a[:, :, 2] @ a[:, :, 1] @ a[:, :, 0] @ a[:, :, 3] @ a[:, :, 2]],
            ])
    # fmt: on

    # Compare.
    assert_allclose(obs_1, obs_2, atol=1e-12)


def test_periodic_schur_1():
    """Test computation of characteristic multipliers."""

    # Example from SLICOT documentation.
    a = np.zeros((4, 4, 2))
    a[:, :, 0] = [
        [1.5, -0.7, 3.5, -0.7],
        [1.0, 0.0, 2.0, 3.0],
        [1.5, -0.7, 2.5, -0.3],
        [1.0, 0.0, 2.0, 1.0],
    ]
    a[:, :, 1] = [
        [1.5, -0.7, 3.5, -0.7],
        [1.0, 0.0, 2.0, 3.0],
        [1.5, -0.7, 2.5, -0.3],
        [1.0, 0.0, 2.0, 1.0],
    ]

    # Build the system.
    sys = LTPdsys(a)

    # Compute characteristic multipliers via matrix multiplication.
    sys.floquet(method="product")
    characteristic_multipliers_prod = sys.characteristic_multipliers

    # Compute characteristic multipliers via periodic Schur decomposition.
    sys.floquet(method="schur_slycot")
    characteristic_multipliers_schur = sys.characteristic_multipliers

    # Compare.
    assert_allclose(characteristic_multipliers_prod, characteristic_multipliers_schur)


# %%

if __name__ == "__main__":
    pass
