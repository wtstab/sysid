"""
Module containing identification algorithms for periodic systems.

References on the Periodic MOESP:

Verhaegen, Michel and Xiaode Yu. "A class of subspace model identification
algorithms to identify periodically and arbitrarily time-varying systems."
Automatica 31.2 (1995): 201-216.

Felici, Federico, Van Wingerden, Jan-Willem, and Verhaegen, Michel. "Subspace
identification of MIMO LPV systems using a periodic scheduling sequence."
Automatica 43.10 (2007): 1684-1697.

Riva, Riccardo. "Stability analysis of wind turbines through system
identification techniques."
PhD Thesis (2018)

References on the Partial Floquet Analysis:

Bauchau, O. A., and Wang, J. (October 24, 2007). "Efficient and Robust Approaches
to the Stability Analysis of Large Multibody Systems." ASME. J. Comput. Nonlinear Dynam.
January 2008; 3(1): 011001. https://doi.org/10.1115/1.2397690

Bauchau, O. A., and Wang, J. (May 1, 2005). "Stability Analysis of Complex Multibody
Systems." ASME. J. Comput. Nonlinear Dynam. January 2006; 1(1): 71–80.
https://doi.org/10.1115/1.1944733


Skjoldan, P. F., and Bauchau, O. A. (March 2, 2011). "Determination of Modal
Parameters in Complex Nonlinear Systems." ASME. J. Comput. Nonlinear Dynam.
July 2011; 6(3): 031017. https://doi.org/10.1115/1.4002975

Wang, J., Shan, X., Wu, B., and Bauchau, O. A. (November 13, 2015). "Time Domain
Approaches to the Stability Analysis of Flexible Dynamical Systems."
ASME. J. Comput. Nonlinear Dynam. July 2016; 11(4): 041003.
https://doi.org/10.1115/1.4031675

Bauchau, O.A., Wang, J. Stability evaluation and system identification of flexible
multibody systems. Multibody Syst Dyn 18, 95–106 (2007).
https://doi.org/10.1007/s11044-007-9067-1

References for applications and many algorithms:
Riva, R., Horcas, S. G., Sørensen, N. N., Grinderslev, C., & Pirrung, G. R. (2022).
Stability analysis of vortex-induced vibrations on wind turbines. In Floating Wind;
Systems Design and Multi-Fidelity/Multi-Disciplinary Modelling; Future Wind; Smaller Wind Turbines
[042054] IOP Publishing. Journal of Physics: Conference Series Vol. 2265 No. 4
https://doi.org/10.1088/1742-6596/2265/4/042054

J. Nathan Kutz, Steven L. Brunton, Bingni W. Brunton, and Joshua L. Proctor. Dynamic
Mode Decomposition. Society for Industrial and Applied Mathematics, Philadelphia, PA,
11 2016.
"""


# %% Import.

import warnings
import numpy as np
from scipy.linalg import lstsq, solve, qr, svd, eig
from scipy.interpolate import interp1d

try:
    from gmpy2 import root
except ModuleNotFoundError:
    pass

from wtsysid.ltp_sys import LTPdsys


# %% Common functions.


def _t_data_mat(t, i, d, period, n_periods):
    r"""
    Create the indices to form the data matrices, denoted in Felici's paper as :math:`\bar{Y}_{i, d, \bar{N}}^{k}`.

    The complete expression is:

    .. math::
        \bar{Y}_{i, d, \bar{N}}^{k} = \begin{pmatrix}
            \boldsymbol{y}(k+i K)      &  \boldsymbol{y}(k+(i+1)K)      &  \cdots  &  \boldsymbol{y}(k+(i+\bar{N}-1)K)        \\
            \boldsymbol{y}(k+i K+1)    &  \boldsymbol{y}(k+(i+1)K+1)    &  \cdots  &  \boldsymbol{y}(k+(i+\bar{N}-1)K+1)      \\
            \vdots                     &  \vdots                        &  \ddots  &  \vdots                                  \\
            \boldsymbol{y}(k+i K+d-1)  &  \boldsymbol{y}(k+(i+1)K+d-1)  &  \cdots  &  \boldsymbol{y}(k+(i+\bar{N}-1)K+d-1)
        \end{pmatrix},

    with:
        - :math:`k` initial time in each period.
        - :math:`i` initial period number.
        - :math:`\bar{N}` final period number.
        - :math:`d` window size.
        - :math:`K` period length.

    It follows that each column in :math:`\bar{Y}_{i, d, \bar{N}}^{k}` contains
    the first :math:`d` instants of a given period between :math:`i` and :math:`\bar{N}`,
    starting from :math:`i`.

    It must be stressed that this is not an Hankel matrix.

    Parameters
    ----------
    t : int
        Initial time.
    i : int
        Initial period.
    d : int
        Past window.
    period: int
        System period.
    n_periods : int
        Number of periods.

    Returns
    -------
    mat : (d, n_periods) ndarray
        Time indices of the data matrix.
    """
    # Row indices.
    row = np.arange(t + i * period, t + i * period + d)

    # Column indices.
    col = np.arange(0, (n_periods - 1) * period + 1, period)

    # Sum row and column indices.
    return row.reshape(-1, 1) + col.reshape(1, -1)


def _sys_x0(nx, ny, past_window, left_singular_vectors, y0d, nu=None, xi=None):
    r"""
    System matrices and initial state.

    Parameters
    ----------
    nx : int
        Number of states.
    ny : int
        Number of output.
    past_window : int
        Past window.
    left_singular_vectors: ndarray
        2D array of left singular vectors.
    y0d : ndarray
        1D array containing the output between 0 and past_window - 1. It is
        denoted as :math:`\bar{y}_0^{\mathit{past\,window}}` in Felici's paper.
    xi : ndarray
        3D array containing the matrix :math:`\boldsymbol{\Xi}(t)` over the period.

    Returns
    -------
    sys : LTPdsys
        Discrete time system containing the :math:`\boldsymbol{A}(t)` and :math:`\boldsymbol{C}(t)` matrices.
        If xi is provided, also the :math:`\boldsymbol{B}(t)` and :math:`\boldsymbol{D}(t)` matrices are computed.
    x0 : ndarray
        1D array with the initial state.
    """
    # System period.
    period = left_singular_vectors.shape[2]

    # Column space of the observability matrix.
    col_space = left_singular_vectors[:, :nx, :]

    # C(t) matrix.
    c = col_space[:ny, :, :]

    # A(t) matrix.
    a = np.zeros((nx, nx, period))
    for t in range(period - 1):
        a[:, :, t], _, _, _ = lstsq(
            col_space[: ny * (past_window - 1), :, t + 1],
            col_space[ny : ny * past_window, :, t],
        )
    a[:, :, period - 1], _, _, _ = lstsq(
        col_space[: ny * (past_window - 1), :, 0],
        col_space[ny : ny * past_window, :, period - 1],
    )

    # Initial state.
    x0, _, _, _ = lstsq(col_space[:, :, 0], y0d)

    # Store system matrices in the system.
    sys = LTPdsys(a, c=c)

    # If needed, compute also B(t) and D(t).
    if xi is not None:
        # Preallocate B and D.
        sys.b = np.zeros((nx, nu, period))
        sys.d = np.zeros((ny, nu, period))

        # Compute another matrix used to get B(t) and D(t).
        # It is named L in Felici's paper.
        gamma = np.zeros((ny * past_window - nx, nx + ny, period, past_window))
        # fmt: off
        for t in range(period):
            for i in range(1, past_window):
                gamma[:, :, t, i] = left_singular_vectors[:, nx:, t].T @ np.block([
                    [np.zeros((ny * (i - 1), nx)),                     np.zeros((ny * (i - 1), ny))          ],
                    [np.zeros((ny, nx)),                               np.identity(ny)                       ],
                    [sys.observability_matrix(t + i, past_window - i), np.zeros((ny * (past_window - i), ny))],
                    ])
        # fmt: on

        # Initialize the left and right hand sides.
        gamma_red = np.zeros((ny * past_window - nx, nx + ny, past_window))
        xi_red = np.zeros((ny * past_window - nx, nu, past_window))

        # Loop over the period.
        for t in range(period):
            # Assemble the left and right hand sides.
            for i in range(past_window):
                gamma_red[:, :, i] = gamma[:, :, t - i, i]
                xi_red[:, :, i] = xi[:, i * nu : (i + 1) * nu, t - i]

            # Solve the least-squares problem.
            bd, _, _, _ = lstsq(
                np.reshape(
                    np.transpose(gamma_red, (0, 2, 1)), (-1, gamma_red.shape[1])
                ),
                np.reshape(np.transpose(xi_red, (0, 2, 1)), (-1, xi_red.shape[1])),
            )

            # Extract B(t) and D(t).
            sys.b[:, :, t] = bd[:nx, :]
            sys.d[:, :, t] = bd[nx:, :]

    # Return.
    return sys, x0


# %% Variants of the periodic MOESP.


def _pmoesp_free(y, nx, period, past_window):
    r"""
    Identify the system

    .. math::
        \boldsymbol{x}(t + 1)
            &= \boldsymbol{A}(t) \boldsymbol{x}(t),
        \qquad\qquad \boldsymbol{x}(0)
                        = \boldsymbol{x}_0,   \\
        \boldsymbol{y}(t)
            &= \boldsymbol{C}(t) \boldsymbol{x}(t)
             + \boldsymbol{v}(t),

    using the Periodic MOESP. Here:

        - :math:`t` is the discrete time index, which goes from 0 to nt.
        - :math:`\boldsymbol{x}(t)` is the state, with :math:`n_x` elements
        - :math:`\boldsymbol{y}(t)` is the output, with :math:`n_y` elements
        - All matrices are periodic with period T, i.e. :math:`\boldsymbol{A}(t+T) = \boldsymbol{A}(t)`.
        - :math:`\boldsymbol{v}(t)` is the measurement noise, assumed white and uncorrelated with
          the initial state :math:`\boldsymbol{x}_0`.

    In particular, there are no input and process noise.

    Parameters
    ----------
    y : (ny, nt) array_like
        Output, with ny the number of output channels and nt the number of time
        instants.
    nx : int
        Estimated number of states of the identified system.
    period: int
        System period.
    past_window : int
        Past window.

    Returns
    -------
    sys : LTPdsys
        The identified system.
    x0 : float ndarray
        Initial state.
    singular_values : float ndarray
        Singular values.
    """
    # Check the past window.
    assert (
        past_window <= period
    ), "The past window must be less than or equal to the period ({}).".format(period)
    assert (
        past_window > nx
    ), "The past window must be greater than the estimated number of states ({}).".format(
        nx
    )

    # Number of output.
    ny = y.shape[0]

    # Number of time indices.
    nt = y.shape[1]

    # Number of periods.
    # The true number of periods is nt // period.
    # Here we compute a reduced number of period, to make sure that at every
    # time index we have enough data.
    n_periods = (nt - past_window + 2) // period

    # Shape of the data matrix.
    data_mat_shape = (ny * past_window, n_periods)

    # Singular values.
    singular_values = np.zeros((min(data_mat_shape), period))
    # Left singular vectors. The second dimension is:
    #   - ny * past_window if full_matrices=True
    #   - min(data_mat_shape) if full_matrices=False
    left_singular_vectors = np.zeros((ny * past_window, min(data_mat_shape), period))

    # Time loop.
    for t in range(period):
        # Indices for the data matrix.
        all_t = _t_data_mat(t, 0, past_window, period, n_periods)

        # Data matrix.
        data_mat = np.transpose(y[:, all_t], (1, 0, 2)).reshape(data_mat_shape)

        # Estimate the column space of observability matrix using the Singular
        # Value Decomposition.
        # full_matrices is set to False because here we don't need to compute
        # B(t) and D(t).
        left_singular_vectors[:, :, t], singular_values[:, t], _ = svd(
            data_mat, full_matrices=False, check_finite=False
        )

    # System matrices and initial state.
    sys, x0 = _sys_x0(
        nx,
        ny,
        past_window,
        left_singular_vectors=left_singular_vectors,
        y0d=y[:, :past_window].reshape(-1),
    )

    # Return.
    return sys, x0, singular_values


def _pmoesp(y, u, nx, period, past_window, zero_mean_input=True):
    r"""
    Identify the system

    .. math::
        \boldsymbol{x}(t + 1)
            &= \boldsymbol{A}(t) \boldsymbol{x}(t)
             + \boldsymbol{B}(t) \boldsymbol{u}(t),
        \qquad\qquad \boldsymbol{x}(0)
                        = \boldsymbol{x}_0,   \\
        \boldsymbol{y}(t)
            &= \boldsymbol{C}(t) \boldsymbol{x}(t)
             + \boldsymbol{D}(t) \boldsymbol{u}(t)
             + \boldsymbol{v}(t),

    using the Periodic MOESP. Here:

        - :math:`t` is the discrete time index, which goes from 0 to nt.
        - :math:`\boldsymbol{x}(t)` is the state, with :math:`n_x` elements
        - :math:`\boldsymbol{u}(t)` is the input, with :math:`n_u` elements
        - :math:`\boldsymbol{y}(t)` is the output, with :math:`n_y` elements
        - All matrices are periodic with period T, i.e. :math:`\boldsymbol{A}(t+T) = \boldsymbol{A}(t)`.
        - :math:`\boldsymbol{v}(t)` is the measurement noise, assumed white and uncorrelated with
          the input and the initial state :math:`\boldsymbol{x}_0`.

    In particular, there is no process noise.

    Parameters
    ----------
    y : (ny, nt) array_like
        Output, with ny the number of output channels and nt the number of time
        instants.
    u : (nu, nt) array_like
        Input, with nu the number of input channels and nt the number of time
        instants.
    nx : int
        Estimated number of states of the identified system.
    period: int
        System period.
    past_window : int
        Past window.
    zero_mean_input : bool
        True if the input has zero mean.

    Returns
    -------
    sys : LTPdsys
        The identified system.
    x0 : float ndarray
        Initial state.
    singular_values : float ndarray
        Singular values.
    """
    # Check the past window.
    assert (
        past_window <= period
    ), "The past window must be less than or equal to the period ({}).".format(period)
    assert (
        past_window > nx
    ), "The past window must be greater than the estimated number of states ({}).".format(
        nx
    )

    # Number of input.
    nu = u.shape[0]

    # Number of output.
    ny = y.shape[0]

    # Number of time indices.
    nt = y.shape[1]

    # Number of periods.
    # The true number of periods is nt // period.
    # Here we compute a reduced number of periods, to make sure that at every
    # time index we have enough data.
    n_periods = (nt - past_window + 2) // period

    # Check the number of periods, to make sure that we can extract the blocks
    # from the LQ decomposition.
    assert n_periods > nu * past_window, (
        "The reduced number of periods must be greater than {}."
        " Alternatively, the past window must be less than {}.".format(
            nu * past_window, int(n_periods // nu)
        )
    )

    # Shape of the data matrix.
    u_mat_shape = (nu * past_window, n_periods)
    y_mat_shape = (ny * past_window, n_periods)
    data_mat_shape = ((nu + ny) * past_window, n_periods)

    # Singular values and left singular vectors.
    # The number of singular values is determined by the shape of l22.
    if data_mat_shape[0] <= data_mat_shape[1]:
        singular_values = np.zeros((ny * past_window, period))

    else:  # data_mat_shape[0] > data_mat_shape[1]:
        singular_values = np.zeros((n_periods - nu * past_window, period))

    left_singular_vectors = np.zeros((ny * past_window, ny * past_window, period))

    # Matrix used to compute B(t) and D(t).
    xi = np.zeros((ny * past_window - nx, past_window * nu, period))

    # Time loop.
    for t in range(period):
        # Indices for the data matrix.
        all_t = _t_data_mat(t, 0, past_window, period, n_periods)

        # Data matrix.
        data_mat = np.row_stack(
            (
                np.transpose(u[:, all_t], (1, 0, 2)).reshape(u_mat_shape),
                np.transpose(y[:, all_t], (1, 0, 2)).reshape(y_mat_shape),
            )
        )

        # LQ decomposition.
        # In r mode we avoid computing Q, and L has the size ((nu + ny) * past_window) x n_periods.
        # - If data_mat has more columns than rows, then the shapes are:
        #   L11   (nu * past_window) x (nu * past_window)
        #   L21   (ny * past_window) x (nu * past_window)
        #   L22   (ny * past_window) x (ny * past_window)
        #   The right block of L, from column (nu + ny) * past_window, is filled with 0.
        # - If data_mat has more rows than columns, then there are no zeros on the right,
        #   and the shape of L22 becomes (ny * past_window) x (n_periods - nu * past_window).
        l_mat = np.transpose(qr(data_mat.T, mode="r", check_finite=False)[0])

        # Extract L11.
        l11 = l_mat[: u_mat_shape[0], : u_mat_shape[0]]

        # Extract L21.
        l21 = l_mat[u_mat_shape[0] :, : u_mat_shape[0]]

        # Extract L22.
        l22 = l_mat[u_mat_shape[0] :, u_mat_shape[0] : data_mat_shape[0]]

        # Estimate the column space of observability matrix using the
        # Singular Value Decomposition.
        left_singular_vectors[:, :, t], singular_values[:, t], _ = svd(
            l22, full_matrices=True, check_finite=False
        )

        # Matrix used to compute B(t) and D(t).
        if zero_mean_input:
            xi[:, :, t] = np.transpose(
                solve(l11.T, l21.T @ left_singular_vectors[:, nx:, t])
            )

        else:
            # If L11 is singular.
            mat, _, _, _ = lstsq(l11.T, l21.T @ left_singular_vectors[:, nx:, t])
            xi[:, :, t] = mat.T

    # System matrices and initial state.
    sys, x0 = _sys_x0(
        nx,
        ny,
        past_window,
        left_singular_vectors=left_singular_vectors,
        y0d=y[:, :past_window].reshape(-1),
        nu=nu,
        xi=xi,
    )

    # Return.
    return sys, x0, singular_values


def _pmoesp_po(y, u, nx, period, past_window, zero_mean_input=True):
    r"""
    Identify the system

    .. math::
        \boldsymbol{x}(t + 1)
            &= \boldsymbol{A}(t) \boldsymbol{x}(t)
             + \boldsymbol{B}(t) \boldsymbol{u}(t)
             + \boldsymbol{F}(t) \boldsymbol{w}(t),
        \quad \boldsymbol{x}(0)
                = \boldsymbol{x}_0,   \\
        \boldsymbol{y}(t)
            &= \boldsymbol{C}(t) \boldsymbol{x}(t)
             + \boldsymbol{D}(t) \boldsymbol{u}(t)
             + \boldsymbol{v}(t),

    using the Periodic PO-MOESP. Here:

        - :math:`t` is the discrete time index, which goes from 0 to nt.
        - :math:`\boldsymbol{x}(t)` is the state, with :math:`n_x` elements
        - :math:`\boldsymbol{u}(t)` is the input, with :math:`n_u` elements
        - :math:`\boldsymbol{y}(t)` is the output, with :math:`n_y` elements
        - All matrices are periodic with period T, i.e. :math:`\boldsymbol{A}(t+T) = \boldsymbol{A}(t)`.
        - :math:`\boldsymbol{v}(t)` is the measurement noise, assumed white and uncorrelated with
          the input and the initial state :math:`\boldsymbol{x}_0`.
        - :math:`\boldsymbol{w}(t)` is the process noise, assumed white and uncorrelated with
          the input and the initial state :math:`\boldsymbol{x}_0`.

    Parameters
    ----------
    y : (ny, nt) array_like
        Output, with ny the number of output channels and nt the number of time
        instants.
    u : (nu, nt) array_like
        Input, with nu the number of input channels and nt the number of time
        instants.
    nx : int
        Estimated number of states of the identified system.
    period: int
        System period.
    past_window : int
        Past window.
    zero_mean_input : bool
        True if the input has zero mean.

    Returns
    -------
    sys : LTPdsys
        The identified system.
    x0 : float ndarray
        Initial state.
    singular_values : float ndarray
        Singular values.
    """
    # Check the past window.
    assert (
        past_window <= period
    ), "The past window must be less than or equal to the period ({}).".format(period)
    assert (
        past_window > nx
    ), "The past window must be greater than the estimated number of states ({}).".format(
        nx
    )

    # Number of input.
    nu = u.shape[0]

    # Number of output.
    ny = y.shape[0]

    # Number of time indices.
    nt = y.shape[1]

    # Number of periods.
    # The true number of periods is nt // period.
    # Here we compute a reduced number of period, to make sure that at every
    # time index we have enough data.
    n_periods = (nt - past_window + 2) // (period + 1) - 1

    # Check the number of periods, to make sure that we can extract the blocks
    # from the LQ decomposition.
    # assert n_periods > (2 * nu + ny) * past_window, (
    #     'The reduced number of periods must be greater than {}.'
    #     ' Alternatively, the past window must be less than {}.'.format(
    #         (2 * nu + ny) * past_window,
    #         int(n_periods // (2 * nu + ny)))
    #     )

    # Check the number of periods, to make sure that we can extract the blocks
    # from the LQ decomposition.
    if n_periods < 2 * (nu + ny) * past_window:
        warnings.warn("There are not enough periods, zero padding is used.")
        # Determine how many time instants to add.
        delta = (2 * (nu + ny) * past_window + 1) * (period + 1) + past_window - 2 - nt
        u = np.hstack((u, np.zeros((nu, delta))))
        y = np.hstack((y, np.zeros((ny, delta))))
        nt = y.shape[1]
        n_periods = (nt - past_window + 2) // (period + 1) - 1

    # Shape of the data matrix.
    u_mat_shape = (nu * past_window, n_periods)
    y_mat_shape = (ny * past_window, n_periods)
    # z_mat_shape = ((nu + ny) * past_window, n_periods)
    # data_mat_shape = (2 * (nu + ny) * past_window, n_periods)

    # Singular values and left singular vectors.
    # The number of singular values is determined by the shape of l32.
    singular_values = np.zeros((ny * past_window, period))
    left_singular_vectors = np.zeros((ny * past_window, ny * past_window, period))

    # Matrices used to compute B(t) and D(t).
    xi = np.zeros((ny * past_window - nx, past_window * nu, period))

    # Time loop.
    for t in range(period):
        # Indices for the data matrix.
        all_t1 = _t_data_mat(t, 1, past_window, period, n_periods)

        # Indices for the instrumental variables data matrix.
        all_t0 = _t_data_mat(
            t + period - past_window, 0, past_window, period, n_periods
        )

        # Data matrix.
        data_mat = np.row_stack(
            (
                np.transpose(u[:, all_t1], (1, 0, 2)).reshape(u_mat_shape),
                np.transpose(u[:, all_t0], (1, 0, 2)).reshape(u_mat_shape),
                np.transpose(y[:, all_t0], (1, 0, 2)).reshape(y_mat_shape),
                np.transpose(y[:, all_t1], (1, 0, 2)).reshape(y_mat_shape),
            )
        )

        # LQ decomposition of data_mat.
        # In r mode we avoid computing Q, and L has size (2 * (nu + ny) * past_window) x n_periods
        # Possibly after the zero padding, L must have more columns than rows.
        # L is split as
        #
        #       --                 --
        #       |  L11  0    0    0 |
        #   L = |  L21  L22  0    0 |
        #       |  L31  L32  L33  0 |
        #       |--                --
        #
        # The diagonal blocks are square, with shape:
        #
        #   - L11   (nu * past_window)      x (nu * past_window)
        #   - L22   ((nu+ny) * past_window) x ((nu+ny) * past_window)
        #   - L33   (ny * past_window)      x (ny * past_window)
        #
        # It follows that the other blocks have shape:
        #
        #   - L21   ((nu+ny) * past_window) x (nu * past_window)
        #   - L31   (ny * past_window)      x (nu * past_window)
        #   - L32   (ny * past_window)      x ((nu + ny) * past_window)
        #
        # The right block of L, from column 2 * (nu + ny) * past_window, is filled with 0.
        l_mat = np.transpose(qr(data_mat.T, mode="r", check_finite=False)[0])

        # Extract L11.
        l11 = l_mat[: u_mat_shape[0], : u_mat_shape[0]]

        # Extract L31.
        l31 = l_mat[(2 * nu + ny) * past_window :, : u_mat_shape[0]]

        # Extract L32.
        l32 = l_mat[
            (2 * nu + ny) * past_window :,
            nu * past_window : (2 * nu + ny) * past_window,
        ]

        # Estimate the column space of observability matrix using the
        # Singular Value Decomposition.
        left_singular_vectors[:, :, t], singular_values[:, t], _ = svd(
            l32, full_matrices=True, check_finite=False
        )

        # Matrix used to compute B(t) and D(t).
        if zero_mean_input:
            xi[:, :, t] = np.transpose(
                solve(l11.T, l31.T @ left_singular_vectors[:, nx:, t])
            )

        else:
            # If L11 is singular.
            mat, _, _, _ = lstsq(l11.T, l31.T @ left_singular_vectors[:, nx:, t])
            xi[:, :, t] = mat.T

    # System matrices and initial state.
    sys, x0 = _sys_x0(
        nx,
        ny,
        past_window,
        left_singular_vectors=left_singular_vectors,
        y0d=y[:, :past_window].reshape(-1),
        nu=nu,
        xi=xi,
    )

    # Return.
    return sys, x0, singular_values


# Wrapper.
def pmoesp(
    y, nx, period, past_window, u=None, process_noise=False, zero_mean_input=True
):
    r"""
    Identify the system

    .. math::
        \boldsymbol{x}(t + 1)
            &= \boldsymbol{A}(t) \boldsymbol{x}(t)
             + \boldsymbol{B}(t) \boldsymbol{u}(t)
             + \boldsymbol{F}(t) \boldsymbol{w}(t),
        \quad \boldsymbol{x}(0)
                = \boldsymbol{x}_0,   \\
        \boldsymbol{y}(t)
            &= \boldsymbol{C}(t) \boldsymbol{x}(t)
             + \boldsymbol{D}(t) \boldsymbol{u}(t)
             + \boldsymbol{v}(t),

    using the Periodic MOESP and its variants. Here:

        - :math:`t` is the discrete time index, which goes from 0 to nt.
        - :math:`\boldsymbol{x}(t)` is the state, with :math:`n_x` elements
        - :math:`\boldsymbol{u}(t)` is the input, with :math:`n_u` elements
        - :math:`\boldsymbol{y}(t)` is the output, with :math:`n_y` elements
        - All matrices are periodic with period T, i.e. :math:`\boldsymbol{A}(t+T) = \boldsymbol{A}(t)`.
        - :math:`\boldsymbol{v}(t)` is the measurement noise, assumed white and uncorrelated with
          the input and the initial state :math:`\boldsymbol{x}_0`.
        - :math:`\boldsymbol{w}(t)` is the process noise, assumed white and uncorrelated with
          the input and the initial state :math:`\boldsymbol{x}_0`.

    Parameters
    ----------
    y : (ny, nt) array_like
        Output, with ny the number of output channels and nt the number of time
        instants.
    u : (nu, nt) array_like
        Input, with nu the number of input channels and nt the number of time
        instants.
    nx : int
        Estimated number of states of the identified system.
    period: int
        System period.
    past_window : int
        Past window.
    u : (nu, nt) array_like
        Input, with nu the number of input channels and nt the number of time
        instants.
    process_noise : boolean
        True if there is process noise.
    zero_mean_input : bool
        True if the input has zero mean.

    Returns
    -------
    sys : LTPdsys
        The identified system.
    x0 : float ndarray
        Initial state.
    singular_values : float ndarray
        Singular values.
    """
    if process_noise:
        res = _pmoesp_po(y, u, nx, period, past_window, zero_mean_input)
    else:
        if u is None:
            res = _pmoesp_free(y, nx, period, past_window)
        else:
            res = _pmoesp(y, u, nx, period, past_window, zero_mean_input)

    return res


# %% Partial Floquet Analysis.


def pfa(y, nx, period, past_window, dt, compute_mode_shapes=False):
    r"""
    Identify the modes of

    .. math::
        \boldsymbol{x}(t + 1)
            &= \boldsymbol{A}(t) \boldsymbol{x}(t),
        \qquad\qquad \boldsymbol{x}(0)
                        = \boldsymbol{x}_0,   \\
        \boldsymbol{y}(t)
            &= \boldsymbol{C}(t) \boldsymbol{x}(t)
             + \boldsymbol{v}(t),

    using the Partial Floquet Analysis. Here:

        - :math:`t` is the discrete time index, which goes from 0 to nt.
        - :math:`\boldsymbol{x}(t)` is the state, with :math:`n_x` elements.
        - :math:`\boldsymbol{y}(t)` is the output, with :math:`n_y` elements.
        - All matrices are periodic with period T, i.e. :math:`\boldsymbol{A}(t+T) = \boldsymbol{A}(t)`.
        - :math:`\boldsymbol{v}(t)` is the measurement noise, assumed white and uncorrelated with
          the input and the initial state :math:`\boldsymbol{x}_0`.

    In particular, the system is autonomus and there is no process noise.


    Parameters
    ----------
    y : (ny, nt) array_like
        Output, with ny the number of output channels and nt the number of time
        instants.
    nx : int
        Estimated number of states of the identified system.
    period: int
        System period.
    past_window : int
        Past window.
    dt : float
        Time step, for the conversion from discrete to continuous time.
    compute_mode_shapes : bool
        `True` to compute observed mode shapes, `False` otherwise.

    Returns
    -------
    natural_frequencies : array_like
        Identified natural frequencies. Harmonics on the rows and modes on the columns.
    damping_ratios : array_like
        Identified damping ratios. Harmonics on the rows and modes on the columns.
    singular_values : (nx) array_like
        Singular values.
    mode_shapes : array_like
        Observed mode shapes. Only returned if compute_mode_shapes = True.
        The indices are ordered as:
            - Index 0 for the harmonics;
            - Index 1 for the modes;
            - Index 2 for the output channels.
    """
    # Check the past window.
    assert (
        past_window <= period
    ), f"The past window must be less than or equal to the period ({period})."
    assert (
        past_window > nx
    ), f"The past window must be greater than the estimated number of states ({nx})."

    # Number of output.
    ny = y.shape[0]

    # Number of time indices.
    nt = y.shape[1]

    # Number of periods.
    # The true number of periods is nt // period.
    # Here we compute a reduced number of period, to make sure that at every
    # time index we have enough data.
    n_periods = (nt - past_window + 2) // period

    # Assemble the data matrix.
    # We first get the indices for all periods, and later we split into matrices
    # that differ by 1 period.
    all_t = _t_data_mat(0, 0, past_window, period, n_periods)
    data_mat = y[:, all_t].transpose(1, 0, 2).reshape(ny * past_window, n_periods)

    data_mat_0 = data_mat[:, :-1]
    data_mat_1 = data_mat[:, 1:]

    # Compute the SVD.
    U, singular_values, Vh = svd(data_mat_0, full_matrices=False, check_finite=False)

    # Truncate to the most energetic modes.
    Ur = U[:, :nx]
    singular_values_r = singular_values[:nx]
    Vr = Vh[:nx, :]

    # Estimate the monodromy matrix.
    monodromy_matrix = np.multiply(Ur.T @ data_mat_1 @ Vr.T, 1.0 / singular_values_r)

    # Do the eigenvalue analysis.
    characteristic_multipliers = eig(monodromy_matrix, right=False, check_finite=False)

    modulus_theta = np.abs(characteristic_multipliers)
    phase_theta = np.angle(characteristic_multipliers)  # [rad]

    # Define the shifts.
    # Put the 0 shift in the middle of the array.
    shift = np.arange(0, period)
    shift0 = period // 2
    shift = np.roll(shift, shift0)

    # Discrete time characteristic exponents.
    # The characteristic exponents are stored in a 2D array.
    #  - row index spans the harmonics, and has length period.
    #  - column index spans the modes, and has length nx.
    try:
        # This requires the gmpy2 package.
        modulus_theta_root = np.zeros_like(modulus_theta)
        for i in range(self.nx):
            modulus_theta_root[i] = float(root(modulus_theta[i], self.period))

    except NameError:
        # Resort to numpy.
        modulus_theta_root = np.power(modulus_theta, 1.0 / period)

    characteristic_exponents = modulus_theta_root.reshape(1, -1) * np.exp(
        1j * (phase_theta.reshape(1, -1) + (2 * np.pi * shift).reshape(-1, 1)) / period
    )

    # Discrete to continuous time conversion.
    characteristic_exponents_continuous = np.log(characteristic_exponents) / dt

    # Frequencies and damping ratios.
    abs_eta = np.abs(characteristic_exponents_continuous)  # [rad/s]
    natural_frequencies = abs_eta / (2 * np.pi)  # [Hz]
    damping_ratios = -np.real(characteristic_exponents_continuous) / abs_eta  # [-]

    # Observed mode shapes.
    if compute_mode_shapes:
        # Build the regressor matrix.
        # Each row in reg is a different time instant.
        # The columns have all the harmonics for mode 0, then all the harmonics for mode 1 and so on.
        reg = characteristic_exponents.reshape(1, -1) ** np.arange(nt).reshape(-1, 1)
        # Solve the least-squares problem to identify the mode shapes.
        # The rows of mode_shapes follow the same order of reg.
        # Each column is a different output channel.
        # Use default lapack driver gelsd, because gelsy is slightly slower in this case.
        mode_shapes, _, _, _ = lstsq(reg, y.T, check_finite=False)
        # Reshape the mode shapes to have:
        # - Index 0 for the harmonics;
        # - Index 1 for the modes;
        # - Index 2 for the output channels.
        mode_shapes = mode_shapes.reshape(period, nx, ny)

        return natural_frequencies, damping_ratios, singular_values, mode_shapes

    else:  # compute_mode_shapes = False
        return natural_frequencies, damping_ratios, singular_values


# %% Dynamic Mode Decomposition (DMD).


def dmd(X, nx):
    r"""
    Identify the DMD modes of

    .. math::
        \boldsymbol{x}(t + 1)
            &= \boldsymbol{A} \boldsymbol{x}(t) + \boldsymbol{B} \boldsymbol{u}_0,
        \qquad\qquad \boldsymbol{x}(0)
                        = \boldsymbol{x}_0,   \\
        \boldsymbol{y}(t)
            &= \boldsymbol{C} \boldsymbol{x}(t)
             + \boldsymbol{v}(t),

    using the Dynamic Mode Decomposition. Here:

        - :math:`t` is the discrete time index, which goes from 0 to nt.
        - :math:`\boldsymbol{x}(t)` is the state, with :math:`n_x` elements
        - :math:`\boldsymbol{u}_0` is the constant input, with :math:`n_u` elements
        - :math:`\boldsymbol{y}(t)` is the output, with :math:`n_y` elements
        - All matrices are constant.
        - :math:`\boldsymbol{v}(t)` is the measurement noise, assumed white and uncorrelated with
          the input and the initial state :math:`\boldsymbol{x}_0`.

    In particular, there is no process noise and the input is constant.

    Implements algorithm 7.2 from Kutz's book on DMD.

    Parameters
    ----------
    X : (m, n) array_like
        Snapshots, where each column is a different snapshot.
    nx : int
        Estimated number of states of the identified system.

    Returns
    -------
    eigen_values : array_like
        Identified eigenvalues.
    dmd_modes : array_like
        Identified DMD modes.
    singular_values :array_like
        Hankel singular values.
    """
    # augment the snapshots to contain snapshot pairs (required for oscillatory
    # real signals)
    X_hankel = np.vstack([X[:, :-1], X[:, 1:]])

    # Separate augmented snapshots into pairs.
    X1 = X_hankel[:, :-1]
    X2 = X_hankel[:, 1:]

    # Compute SVD.
    U, singular_values, V = svd(X1, full_matrices=False, check_finite=False)
    V = V.conj().T
    rank = min(nx, U.shape[1])
    U = U[:, :rank]
    singular_values = singular_values[:rank]
    V = V[:, :rank]

    # Since S = np.diag(singular_values), compute Sinv_{ii} = 1/S_{ii}, Sinv_{ij} = 0.0
    Sinv = np.diag(1.0 / singular_values)

    # Calculate the lower dimensional transition matrix.
    A_tilde = U.T.conj() @ X2 @ V @ Sinv

    # Calculate the lower dimensional eigenmodes and eigenvalues.
    eigen_values, eigen_vectors = eig(A_tilde, check_finite=False)

    # Project eigenmodes to higher dimensional space.
    dmd_modes = X2 @ V @ Sinv @ eigen_vectors

    return eigen_values, dmd_modes, singular_values


# %% Half-Power Bandwidth.


def half_power_bandwidth(psd_frequency, psd_magnitude, mode_frequencies):
    r"""
    Identify the damping ratio using the half-power bandwidth method.

    Parameters
    ----------
    psd_frequency : (nf,) array_like
        Frequency array of the Power Spectral Densities.

    psd_magnitude : (nf, nc) array_like
        2D array containing the magnitude of the Power Spectral Densities.
        Each row is associated to a different frequency, and each column to a different output channel.

    mode_frequencies : (nm,) array_like
        Frequencies of the modes for which the damping ratio should be computed.

    Returns
    -------
    damping_ratios : (nm, nc) array_like
        Identified damping ratios for each mode and output channel.
    """
    # This function is inspired by peak_widths() in scipy.
    # The use of interp1 is limited because in scipy and numpy it sorts the array of the independent variable.
    # Get a column vector if the input is 1D.
    if psd_magnitude.ndim == 1:
        psd_magnitude = psd_magnitude.reshape(-1, 1)

    # Interpolate the Power Spectral Density (PSD).
    interpolant = interp1d(
        psd_frequency, psd_magnitude, axis=0, copy=False, assume_sorted=True
    )

    # Evaluate the PSD at the given frequencies.
    # Shape (nm, nc)
    peak_magnitudes = interpolant(mode_frequencies)

    # Get the half-power.
    # Shape (nm, nc)
    half_peak_magnitudes = peak_magnitudes / 2.0

    # Get the indices of mode_frequencies.
    # Shape (nm,)
    i_mode_frequencies = np.searchsorted(psd_frequency, mode_frequencies)

    # Preallocate the array for the damping ratios.
    damping_ratios = np.zeros((mode_frequencies.shape[0], psd_magnitude.shape[1]))

    # Loop over the output channels.
    for i_c in range(psd_magnitude.shape[1]):
        # Arrays for the left and right frequencies.
        mode_frequencies_left = np.zeros((mode_frequencies.shape[0]))
        mode_frequencies_right = np.zeros((mode_frequencies.shape[0]))
        # Loop over the modes.
        for i_m in range(mode_frequencies.shape[0]):
            # Look for the left index.
            i_left = i_mode_frequencies[i_m] - 1
            while psd_magnitude[i_left, i_c] > half_peak_magnitudes[i_m, i_c]:
                i_left -= 1
                if i_left == 0:
                    break

            # Look for the right index.
            i_right = i_mode_frequencies[i_m] + 1
            while psd_magnitude[i_right, i_c] > half_peak_magnitudes[i_m, i_c]:
                i_right += 1
                if i_right == psd_frequency.shape[0]:
                    break

            # Interpolate to get the half-power frequencies.
            i_left_p1 = i_left + 1
            i_right_m1 = i_right - 1
            mode_frequencies_left[i_m] = psd_frequency[i_left] + (
                half_peak_magnitudes[i_m, i_c] - psd_magnitude[i_left, i_c]
            ) / (psd_magnitude[i_left_p1, i_c] - psd_magnitude[i_left, i_c]) * (
                psd_frequency[i_left_p1] - psd_frequency[i_left]
            )
            mode_frequencies_right[i_m] = psd_frequency[i_right_m1] + (
                half_peak_magnitudes[i_m, i_c] - psd_magnitude[i_right_m1, i_c]
            ) / (psd_magnitude[i_right, i_c] - psd_magnitude[i_right_m1, i_c]) * (
                psd_frequency[i_right] - psd_frequency[i_right_m1]
            )

        # Get the half-power frequencies.
        damping_ratios[:, i_c] = (mode_frequencies_right - mode_frequencies_left) / (
            2.0 * mode_frequencies
        )

    return damping_ratios
