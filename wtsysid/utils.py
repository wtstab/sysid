"""
Functions that are useful in various cases.
"""

# %% Import.

import numpy as np


# %% Functions.


def get_coleman_rotating_to_fixed(psi):
    """
    Return the Coleman transformation matrix, from rotating to fixed frame.

    Parameters
    ----------
    psi : ndarray
        Array of azimuth angles in rad. Each row is a time instant, and each
        column is a different blade.

    Returns
    -------
    col : ndarray
        3D array containing the Coleman transformation for each time instant.
        The dimensions are ordered as:
            - dimension 0: time
            - dimension 1 and 2: elements of the Coleman matrix.
    """
    # Number of blades.
    n_blades = psi.shape[1]

    if n_blades % 2 != 0:
        # Odd number of blades.
        n_blades_tilde = int((n_blades - 1) / 2)
    else:
        # Even number of blades.
        n_blades_tilde = int((n_blades - 2) / 2)

    # Coleman matrix.
    col = np.ones((psi.shape[0], n_blades, n_blades))
    for b in range(1, n_blades_tilde + 1):
        col[:, 2 * b - 1, :] = 2.0 * np.cos(b * psi)
        col[:, 2 * b, :] = 2.0 * np.sin(b * psi)

    if n_blades % 2 == 0:
        col[:, -1, 0:-1:2] = -1.0

    col /= n_blades

    return col


def apply_coleman_rotating_to_fixed(psi, y):
    """
    Convert values in Multi-Blade Coordinates by using the Coleman transformation.

    Parameters
    ----------
    psi : ndarray
        Array of azimuth angles in rad. Each row is a time instant, and each
        column is a different blade.
    y : ndarray
        3D array of values in blade coordinates. The dimensions are ordered as:
            - dimension 0: time
            - dimension 1: blade coordinate, from 0 till n_blades - 1
            - dimension 2: channel

    Returns
    -------
    y_mbc : ndarray
        3D array of values in Multi-Blade Coordinates. The dimensions are ordered as:
            - dimension 0: time
            - dimension 1: multi-blade coordinates, ordered as:
                    - average
                    - 1 cosine
                    - 1 sine
                    - 2 cosine
                    - 2 sine
                    - ...
                    - n_blades / 2 (only for even number of blades)
            - dimension 2: channel
    """
    # Get the Coleman transformation.
    col = get_coleman_rotating_to_fixed(psi)

    # Apply the Coleman transformation.
    # Same as y_mbc = np.einsum('tmb,tbc->tmc', col, y)
    # %timeit shows that @ is faster.
    y_mbc = col @ y

    return y_mbc
