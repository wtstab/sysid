"""
Mathieu oscillator subject to a constant input.
- Stability analysis.
- System identification.
"""


# %% General import.

import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.patches import Circle

from wtsysid.identification import pmoesp

plt.close("all")


# %% Time integration for the continuous time system.

# System for the Mathieu oscillator.
from wtsysid.examples.mathieu import mathieu

# Input.
u = np.array([0.3])

# Initial condition.
x0 = np.array([1.0, 0.0])

# Time array.
# simulate 100 periods
time_end = mathieu.period * 200
n_points_per_period = 400

# Simulation.
time, y = mathieu.simulate(time_end, n_points_per_period, x0=x0, u=u)

# Frequency properties of simulated input and output.
dt = np.mean(np.diff(time))
df = 1 / (time[-1] - time[0])
sampling_frequency = 1 / dt  # [Hz]
nyquist_frequency = sampling_frequency / 2  # [Hz]


# %% Identification.

# Downsample the simulated output.
downsample_step = 2

# Digital anti-aliasing filter.
cutoff_frequency = nyquist_frequency / downsample_step  # [Hz]
sos = signal.butter(
    N=2, Wn=1.0 / downsample_step, btype="low", analog=False, output="sos"
)

# Apply the filter.
y_filtered = signal.sosfiltfilt(sos, y)

# Decimate.
time_ident = time[0:-1:downsample_step]
dt_ident = np.mean(np.diff(time_ident))
y_ident = y[:, 0:-1:downsample_step]
period_ident = int(np.round(n_points_per_period / downsample_step))

# Frequency properties of downsampled input and output.
df_ident = 1 / (time_ident[-1] - time_ident[0])
sampling_frequency_ident = 1 / dt_ident  # [Hz]
nyquist_frequency_ident = sampling_frequency_ident / 2  # [Hz]

# Create the input.
u_ident = np.broadcast_to(u, (1, time_ident.size))

# Identify.
past_window = 50

sys_ident, x0_ident, singular_values = pmoesp(
    y_ident,
    nx=2,
    period=period_ident,
    past_window=past_window,
    u=u_ident,
    zero_mean_input=False,
)
sys_ident.dt = dt_ident

# Simulate.
y_predicted = sys_ident.simulate(time_ident.size, x0_ident, u=u_ident)


# %% Power Spectral Density for original and identified output.

# Frequency properties.
dt = np.mean(np.diff(time))
df = 1 / (time[-1] - time[0])
sampling_frequency = 1 / dt  # [Hz]
nyquist_frequency = sampling_frequency / 2  # [Hz]

# Parameters.
window = np.ones(len(time) - 1)
noverlap = 0
nperseg = len(window)
nfft = nperseg
detrend = "constant"
return_onesided = True
scaling = "density"

# PSD.
y_frequency, y_PSD = signal.welch(
    y[0, :-1].T,
    sampling_frequency,
    window,
    nperseg,
    noverlap,
    nfft,
    detrend,
    return_onesided,
    scaling,
    axis=0,
)

# Frequency properties.
df_ident = 1 / (time_ident[-1] - time_ident[0])
sampling_frequency_ident = 1 / dt_ident  # [Hz]
nyquist_frequency_ident = sampling_frequency_ident / 2  # [Hz]

# Parameters.
window_ident = np.ones(len(time_ident) - 1)
nperseg_ident = len(window_ident)
nfft_ident = nperseg_ident

# PSD.
y_frequency_ident, y_ident_PSD = signal.welch(
    y_ident[0, :-1].T,
    sampling_frequency_ident,
    window_ident,
    nperseg_ident,
    noverlap,
    nfft_ident,
    detrend,
    return_onesided,
    scaling,
    axis=0,
)

y_frequency_ident, y_predicted_PSD = signal.welch(
    y_predicted[0, :-1].T,
    sampling_frequency_ident,
    window_ident,
    nperseg_ident,
    noverlap,
    nfft_ident,
    detrend,
    return_onesided,
    scaling,
    axis=0,
)


# %% Continuous to discrete time conversion.

sysd = mathieu.c2d(n_points_per_period)


if False:
    fig, ax = plt.subplots(num="Discrete time A[t] matrix")
    ax.set_xlabel("Discrete time [-]")
    ax.set_ylabel("Coefficients")
    ax.grid()
    for i in [0, 1]:
        for j in [0, 1]:
            ax.plot(sysd.a[i, j, :], label="A[{}, {}]".format(i, j))
    ax.legend()


# %% Simulate discrete time system.

## Time simulation.
# yd = sysd.simulate(time_end=time.size, x0=x0)
#
## PSD.
# yd_frequency, yd_PSD = signal.welch(yd[0, :-1].T,
#                                    sampling_frequency,
#                                    window, nperseg, noverlap, nfft, detrend,
#                                    return_onesided, scaling, axis=0)


# %% Stability analysis of discrete time system.

# Floquet stability analysis of the discrete and identified systems.
sysd.floquet()
sys_ident.floquet()

# Print.
sys_ident.print_floquet_results()

if False:
    fig, ax = plt.subplots(num="Transition matrix")
    ax.set_xlabel("Discrete time [-]")
    ax.set_ylabel("Coefficients")
    ax.grid()
    for i in [0, 1]:
        for j in [0, 1]:
            ax.plot(sysd.tm[i, j, :], label="tm[{}, {}]".format(i, j))
    ax.legend()

    fig, ax = plt.subplots(num="Periodic transformation")
    ax.set_xlabel("Discrete time [-]")
    ax.set_ylabel("Coefficients")
    ax.grid()
    for i in [0, 1]:
        for j in [0, 1]:
            ax.plot(sysd.p[i, j, :], label="tm[{}, {}]".format(i, j))
    ax.legend()


# %% Plot.

mpl.rcParams["axes.grid"] = True
mpl.rcParams["grid.linewidth"] = 0.5
mpl.rcParams["font.size"] = 8
mpl.rcParams["axes.titlesize"] = 9
mpl.rcParams["axes.labelsize"] = 9
mpl.rcParams["legend.fontsize"] = 9
mpl.rcParams["axes.labelweight"] = "bold"
mpl.rcParams["axes.titleweight"] = "bold"
mpl.rcParams["xtick.labelsize"] = 8
mpl.rcParams["ytick.labelsize"] = 8
mpl.rcParams["lines.linewidth"] = 2

# Page dimensions.
page_width_pt = 372.0 - 10.0
page_height_pt = 601.90594 + 10
page_width_in = page_width_pt / 72.27
page_height_in = page_height_pt / 72.27

fig, ax = plt.subplots(num="Mathieu - Time")
ax.set_xlabel("Time [s]")
ax.set_ylabel("Displacement [m]")
# ax.grid()
ax.plot(time, y[0, :], label="Continuous")
# ax.plot(time, yd[0, :], label='Discrete')
ax.plot(time_ident, y_ident[0, :], label="Downsampled")
ax.plot(time_ident, y_predicted[0, :], label="Identified")
ax.legend()

fig, ax = plt.subplots(
    num="Mathieu - PSD", figsize=(page_width_in, page_width_in / np.sqrt(2)), dpi=300
)
ax.set_xlabel("Frequency [Hz]")
ax.set_ylabel(r"Displacement [$\mathregular{m^2/Hz}$]")
# ax.grid()
ax.set_yscale("log")
ax.set_xlim(0.0, 0.8)
ax.set_ylim(1e-5, 1e2)
ax.plot(y_frequency, y_PSD, label="Continuous")
# ax.plot(yd_frequency, yd_PSD, label='Discrete')
# ax.plot(y_frequency_ident, y_ident_PSD, label='Downsampled')
# ax.plot(y_frequency_ident, y_predicted_PSD, label='Identified')
ax.legend()
fig.savefig("Mathieu_constant_PSD.pdf", bbox_inches="tight")

fig, ax = plt.subplots(num="Characteristic multipliers")
ax.set_xlabel("Real [-]")
ax.set_ylabel("Imag [-]")
ax.axis("equal")
# ax.grid()
ax.add_patch(Circle((0.0, 0.0), 1.0, facecolor="none", edgecolor="k"))
ax.scatter(
    np.real(sysd.characteristic_multipliers),
    np.imag(sysd.characteristic_multipliers),
    label="Discrete",
)
ax.scatter(
    np.real(sys_ident.characteristic_multipliers),
    np.imag(sys_ident.characteristic_multipliers),
    label="Identified",
)
ax.legend()

fig, ax = plt.subplots(num="Matrices of identified system")
ax.set_xlabel("Discrete time [-]")
ax.set_ylabel("Coefficients")
# ax.grid()
for i in [0, 1]:
    for j in [0, 1]:
        ax.plot(sys_ident.a[i, j, :], label="a[{}, {}]".format(i, j))
for j in [0, 1]:
    ax.plot(sys_ident.c[0, j, :], label="c[0, {}]".format(j))
ax.legend()

if False:
    fig, ax = plt.subplots(num="Matrices of discrete time system")
    ax.set_xlabel("Discrete time [-]")
    ax.set_ylabel("Coefficients")
    # ax.grid()
    for i in [0, 1]:
        for j in [0, 1]:
            ax.plot(sys_ident.a[i, j, :], label="a[{}, {}]".format(i, j))
    for j in [0, 1]:
        ax.plot(sys_ident.c[0, j, :], label="c[0, {}]".format(j))
    ax.legend()

fig, ax = plt.subplots(num="Singular values")
ax.set_xlabel("Number [-]")
ax.set_ylabel("Singular values")
# ax.grid()
ax.set_yscale("log")
for t in range(singular_values.shape[0]):
    ax.scatter(np.full((singular_values.shape[1]), t), singular_values[t, :])
# ax.scatter(np.tile(np.arange(singular_values.shape[0]).reshape(-1, 1), (1, singular_values.shape[1])),
#           singular_values)
