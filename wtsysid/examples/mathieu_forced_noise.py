"""
Mathieu oscillator forced with a white noise input.
- Stability analysis.
- System identification.
"""


# %% General import.

import numpy as np
from scipy import signal
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.patches import Circle

from wtsysid.ltp_sys import build_time_array
from wtsysid.identification import pmoesp

plt.close("all")


# %% System for the Mathieu oscillator.

from wtsysid.examples.mathieu import mathieu

# Initial condition.
x0 = np.array([0.0, 0.0])

# Time array.
time_end = mathieu.period * 100
n_points_per_period = 400
time, dt, dpsi = build_time_array(0.0, time_end, mathieu.period, n_points_per_period)

# Frequency properties.
df = 1 / (time[-1] - time[0])
sampling_frequency = 1 / dt  # [Hz]
nyquist_frequency = sampling_frequency / 2  # [Hz]


# %% Input.

# Number of experiments.
n_experiments = 10

# Lowpass filter up to 2 Hz.
cutoff_frequency = 4.0  # [Hz]
sos_u = signal.butter(
    N=4,
    Wn=cutoff_frequency / nyquist_frequency,
    btype="low",
    analog=False,
    output="sos",
)

# White noise up to 2 Hz.
generator = np.random.RandomState(0)
u = np.zeros((len(time), n_experiments))
for i in range(n_experiments):
    u[:, i] = generator.normal(0.3, 1.0, (len(time)))
u = signal.sosfiltfilt(sos_u, u, axis=0)
u_mean = u.mean(axis=0)


# %% Time integration for the continuous time system.

y = np.zeros((len(time), n_experiments))
for i in range(n_experiments):
    print("Simulating {}/{}".format(i + 1, n_experiments))

    # Make the noise continuous.
    def input(t):
        return np.interp(np.array([[t]]), time, u[:, i])

    time, y[:, i] = mathieu.simulate(t_eval=time, x0=x0, u=input)


# %% Identification.

# Downsample the simulated output.
downsample_step = 5

# Digital anti-aliasing filter.
sos_ident = signal.butter(
    N=2, Wn=1.0 / downsample_step, btype="low", analog=False, output="sos"
)

# Apply the filter.
# u_filtered = signal.sosfiltfilt(sos_ident, u, axis=0)
y_filtered = signal.sosfiltfilt(sos_ident, y, axis=0)

# Decimate.
time_ident = time[0:-1:downsample_step]
dt_ident = np.mean(np.diff(time_ident))
# u_ident = u_filtered[0:-1:downsample_step, :]
y_ident = y_filtered[0:-1:downsample_step, :]
period_ident = int(np.round(n_points_per_period / downsample_step))

# Frequency properties of downsampled input and output.
df_ident = 1 / (time_ident[-1] - time_ident[0])
sampling_frequency_ident = 1 / dt_ident  # [Hz]
nyquist_frequency_ident = sampling_frequency_ident / 2  # [Hz]

# Identify.
past_window = 10

sys_ident_ok = []
x0_ident_ok = []
singular_values_ok = []
sys_ident_wrong = []
x0_ident_wrong = []
singular_values_wrong = []

for i in range(n_experiments):
    # Create the input.
    u_ident = np.broadcast_to(u_mean[i], (1, time_ident.size))

    sys_ident, x0_ident, singular_values = pmoesp(
        y_ident[:, i].reshape(1, -1),
        nx=2,
        period=period_ident,
        past_window=past_window,
        u=u_ident,
        process_noise=True,
        zero_mean_input=False,
    )
    sys_ident.dt = dt_ident
    sys_ident.floquet()

    sys_ident_ok.append(sys_ident)
    x0_ident_ok.append(x0_ident)
    singular_values_ok.append(singular_values)
    del sys_ident, x0_ident, singular_values

    sys_ident, x0_ident, singular_values = pmoesp(
        y_ident[:, i].reshape(1, -1),
        nx=2,
        period=period_ident,
        past_window=past_window,
        u=u_ident,
        process_noise=False,
        zero_mean_input=False,
    )
    sys_ident.dt = dt_ident
    sys_ident.floquet()

    sys_ident_wrong.append(sys_ident)
    x0_ident_wrong.append(x0_ident)
    singular_values_wrong.append(singular_values)
    del sys_ident, x0_ident, singular_values

# Simulate.
# y_predicted = sys_ident.simulate(time_ident.size, x0_ident, u=u_ident)


# %% Power Spectral Density for original and identified output.

# Parameters.
window = "hanning"
nperseg = len(time) // 10
noverlap = nperseg // 2
nfft = nperseg
detrend = "constant"
return_onesided = True
scaling = "density"

# PSD of the input and output.
u_frequency, u_PSD = signal.welch(
    u,
    sampling_frequency,
    window,
    nperseg,
    noverlap,
    nfft,
    detrend,
    return_onesided,
    scaling,
    axis=0,
)

y_frequency, y_PSD = signal.welch(
    y[:-1, :],
    sampling_frequency,
    window,
    nperseg,
    noverlap,
    nfft,
    detrend,
    return_onesided,
    scaling,
    axis=0,
)

# Frequency properties.
# df_ident = 1 / (time_ident[-1] - time_ident[0])
# sampling_frequency_ident = 1/dt_ident   # [Hz]
# nyquist_frequency_ident = sampling_frequency_ident / 2   # [Hz]

# # Parameters.
# window_ident = 'hanning'
# nperseg_ident = len(time_ident) // 10
# noverlap_ident = nperseg_ident // 2
# nfft_ident = nperseg_ident

# # PSD.
# y_frequency_ident, y_ident_PSD = signal.welch(
#         y_ident[0, :-1].T,
#         sampling_frequency_ident,
#         window_ident, nperseg_ident, noverlap_ident, nfft_ident, detrend,
#         return_onesided, scaling, axis=0)

# y_frequency_ident, y_predicted_PSD = signal.welch(
#         y_predicted[0, :-1].T,
#         sampling_frequency_ident,
#         window_ident, nperseg_ident, noverlap_ident, nfft_ident, detrend,
#         return_onesided, scaling, axis=0)


# %% Continuous to discrete time conversion.

sysd = mathieu.c2d(n_points_per_period)


if False:
    fig, ax = plt.subplots(num="Discrete time A[t] matrix")
    ax.set_xlabel("Discrete time [-]")
    ax.set_ylabel("Coefficients")
    ax.grid()
    for i in [0, 1]:
        for j in [0, 1]:
            ax.plot(sysd.a[i, j, :], label="A[{}, {}]".format(i, j))
    ax.legend()


# %% Simulate discrete time system.

## Time simulation.
# yd = sysd.simulate(time_end=time.size, x0=x0)
#
## PSD.
# yd_frequency, yd_PSD = signal.welch(yd[0, :-1].T,
#                                    sampling_frequency,
#                                    window, nperseg, noverlap, nfft, detrend,
#                                    return_onesided, scaling, axis=0)


# %% Stability analysis of discrete time system.

sysd.floquet()

if False:
    fig, ax = plt.subplots(num="Transition matrix")
    ax.set_xlabel("Discrete time [-]")
    ax.set_ylabel("Coefficients")
    ax.grid()
    for i in [0, 1]:
        for j in [0, 1]:
            ax.plot(sysd.tm[i, j, :], label="tm[{}, {}]".format(i, j))
    ax.legend()

    fig, ax = plt.subplots(num="Periodic transformation")
    ax.set_xlabel("Discrete time [-]")
    ax.set_ylabel("Coefficients")
    ax.grid()
    for i in [0, 1]:
        for j in [0, 1]:
            ax.plot(sysd.p[i, j, :], label="tm[{}, {}]".format(i, j))
    ax.legend()


# %% Plot.

mpl.rcParams["axes.grid"] = True
mpl.rcParams["grid.linewidth"] = 0.5
mpl.rcParams["font.size"] = 8
mpl.rcParams["axes.titlesize"] = 9
mpl.rcParams["axes.labelsize"] = 9
mpl.rcParams["legend.fontsize"] = 9
mpl.rcParams["axes.labelweight"] = "bold"
mpl.rcParams["axes.titleweight"] = "bold"
mpl.rcParams["xtick.labelsize"] = 8
mpl.rcParams["ytick.labelsize"] = 8
mpl.rcParams["lines.linewidth"] = 2

colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]

# Page dimensions.
page_width_pt = 372.0 - 10.0
page_height_pt = 601.90594 + 10
page_width_in = page_width_pt / 72.27
page_height_in = page_height_pt / 72.27


# Plot the frequency response of the filter.
frequency_4_filter_plot = np.linspace(0.0, 10.0, 1001)
_, filter_frequency_response = signal.sosfreqz(
    sos_u, worN=frequency_4_filter_plot, fs=sampling_frequency
)

fig, ax = plt.subplots(num="Butterworth filter frequency response")
ax.set_xlabel("Frequency [Hz]")
ax.set_ylabel("Amplitude [dB]")
# ax.grid()
ax.plot(frequency_4_filter_plot, 20 * np.log10(abs(filter_frequency_response)))
plt.axvline(cutoff_frequency, linestyle="--", color="#ff7f0e")

# Plot the PSD of the input.
fig, ax = plt.subplots(
    num="Input PSD", figsize=(page_width_in, page_width_in / np.sqrt(2)), dpi=300
)
ax.set_xlabel("Frequency [Hz]")
ax.set_ylabel(r"Input [$\mathregular{N^2/Hz}$]")
# ax.grid()
ax.set_yscale("log")
ax.set_xlim(0, 10.0)
ax.set_ylim(1e-5, 1e0)
ax.plot(u_frequency, u_PSD)
fig.savefig("White_noise_after_low_pass_filter_PSD.pdf", bbox_inches="tight")

fig, ax = plt.subplots(num="Mathieu - Time")
ax.set_xlabel("Time [s]")
ax.set_ylabel("Displacement [m]")
# ax.grid()
ax.plot(time, y, label="Continuous")
# ax.plot(time, yd[0, :], label='Discrete')
# ax.plot(time_ident, y_ident[0, :], label='Downsampled')
# ax.plot(time_ident, y_predicted[0, :], label='Identified')
# ax.legend()

fig, ax = plt.subplots(
    num="Mathieu - PSD", figsize=(page_width_in, page_width_in / np.sqrt(2)), dpi=300
)
ax.set_xlabel("Frequency [Hz]")
ax.set_ylabel(r"Displacement [$\mathregular{m^2/Hz}$]")
# ax.grid()
ax.set_yscale("log")
ax.set_xlim(0.0, 0.8)
ax.set_ylim(1e-5, 1e2)
ax.plot(y_frequency, y_PSD, label="Continuous")
# ax.plot(yd_frequency, yd_PSD, label='Discrete')
# ax.plot(y_frequency_ident, y_ident_PSD, label='Downsampled')
# ax.plot(y_frequency_ident, y_predicted_PSD, label='Identified')
# ax.legend()
fig.savefig("Mathieu_forced_noise_PSD.pdf", bbox_inches="tight")

fig, ax = plt.subplots(
    num="Characteristic multipliers",
    figsize=(0.8 * page_width_in, 0.8 * page_width_in),
    dpi=300,
)
ax.set_xlabel("Real [-]")
ax.set_ylabel("Imag [-]")
ax.axis("equal")
# ax.grid()
ax.add_patch(Circle((0.0, 0.0), 1.0, facecolor="none", edgecolor="k", zorder=10))
ax.scatter(
    np.real(sys_ident_ok[0].characteristic_multipliers),
    np.imag(sys_ident_ok[0].characteristic_multipliers),
    marker="o",
    c=colors[0],
    label="Periodic PO-MOESP",
)
ax.scatter(
    np.real(sys_ident_wrong[0].characteristic_multipliers),
    np.imag(sys_ident_wrong[0].characteristic_multipliers),
    marker="*",
    c=colors[1],
    label="Standard Periodic MOESP",
)
for i in range(1, n_experiments):
    ax.scatter(
        np.real(sys_ident_ok[i].characteristic_multipliers),
        np.imag(sys_ident_ok[i].characteristic_multipliers),
        marker="o",
        c=colors[0],
    )
    ax.scatter(
        np.real(sys_ident_wrong[i].characteristic_multipliers),
        np.imag(sys_ident_wrong[i].characteristic_multipliers),
        marker="*",
        c=colors[1],
    )
ax.scatter(
    np.real(sysd.characteristic_multipliers),
    np.imag(sysd.characteristic_multipliers),
    s=100,
    marker="+",
    c=colors[3],
    label="Exact",
)
ax.legend()
fig.savefig("Mathieu_noise_characteristic_multipliers.pdf", bbox_inches="tight")

# fig, ax = plt.subplots(num='Matrices of identified system')
# ax.set_xlabel('Discrete time [-]')
# ax.set_ylabel('Coefficients')
# # ax.grid()
# for i in [0, 1]:
#     for j in [0, 1]:
#         ax.plot(sys_ident.a[i, j, :], label='a[{}, {}]'.format(i, j))
# for i in [0, 1]:
#     ax.plot(sys_ident.b[i, 0, :], label='b[{}, 0]'.format(j))
# for j in [0, 1]:
#     ax.plot(sys_ident.c[0, j, :], label='c[0, {}]'.format(j))
# ax.plot(sys_ident.d[0, 0, :], label='d[0, 0]')
# ax.legend()

# fig, ax = plt.subplots(num='Singular values')
# ax.set_xlabel('Number [-]')
# ax.set_ylabel('Singular values')
# # ax.grid()
# ax.set_yscale('log')
# for t in range(singular_values.shape[0]):
#     ax.scatter(np.full((singular_values.shape[1]), t), singular_values[t, :])
# #ax.scatter(np.tile(np.arange(singular_values.shape[0]).reshape(-1, 1), (1, singular_values.shape[1])),
# #           singular_values)
