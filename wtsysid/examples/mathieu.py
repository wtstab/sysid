"""
Equation of motion of a Mathieu oscillator.
"""


# %% Import.

import numpy as np

from wtsysid.ltp_sys import LTPcsys


# %% Define a class for the  Mathieu oscillator.


class Mathieu(LTPcsys):
    r"""
    Provides a Mathieu oscillator, implemented according to
    
    Matthew S. Allen, Michael W. Sracic, Shashank Chauhan, Morten Hartvig Hansen,
    Output-only modal analysis of linear time-periodic systems with application
    to wind turbine simulation data,
    Mechanical Systems and Signal Processing,
    Volume 25, Issue 4,
    2011,
    Pages 1174-1191,
    ISSN 0888-3270,
    https://doi.org/10.1016/j.ymssp.2010.12.018
    
    The equation of motion is
    
    .. math::
        \begin{pmatrix}
            \dot{x}   \\
            \ddot{x}
        \end{pmatrix} &= \begin{bmatrix}
             0                                        &   1                   \\
            -\omega_0^2 - \omega_1^2 \cos(\Omega t)   &   -2\zeta\omega_0
        \end{bmatrix} \begin{pmatrix}
            x        \\
            \dot{x}
        \end{pmatrix} + \begin{bmatrix}
            0     \\
            1/m
        \end{bmatrix} u
        %
        \\
        %
        y &= \begin{bmatrix}
            1   &   0
        \end{bmatrix} \begin{pmatrix}
            x        \\
            \dot{x}
        \end{pmatrix}

    """

    def __init__(self, m=1.0, k0=1.0, k1=0.4, damp=0.04, omega_A=0.8):
        r"""
        Instantiate a Mathieu oscillator.

        Parameters
        ----------
        m : float, optional
            Mass :math:`m`. The default is 1.0.
        k0 : float, optional
            Constant stiffness :math:`k_0`. The default is 1.0.
        k1 : float, optional
            Periodic stiffness :math:`k_1`. The default is 0.4.
        damp : float, optional
            Damping :math:`c`. The default is 0.04.
        omega_A : float, optional
            Frequency of the spring :math:`\omega_A` in [rad/s]. The default is 0.8.

        Returns
        -------
        None.

        """
        self._m = m
        self._k0 = k0
        self._k1 = k1
        self._damp = damp
        self._omega_A = omega_A
        self.update_dependent_parameters()
        super().__init__(
            period=self._T_A, a=self.a_mat_fun, b=self.b_mat_fun, c=self.c_mat_fun
        )

    def update_dependent_parameters(self):
        r"""
        Updates the dependent parametes
        :math:`T_A`, :math:`\omega_0^2`, :math:`\omega_1^2` and :math:`2 \zeta \omega_0`.

        Returns
        -------
        None.

        """
        self._T_A = 2 * np.pi / self._omega_A  # [s]
        self._omega_02 = self._k0 / self._m  # =omega_0^2
        self._omega_12 = self._k1 / self._m  # =omega_1^2
        self._cc = self._damp / self._m  # =2*zeta*omega_0

    @property
    def m(self):
        r"""
        Mass :math:`m`.
        """
        return self._m

    @m.setter
    def m(self, value):
        self._m = value
        self.update_dependent_parameters()

    @property
    def k0(self):
        r"""
        Constant stiffness :math:`k_0`.
        """
        return self._k0

    @k0.setter
    def k0(self, value):
        self._k0 = value
        self.update_dependent_parameters()

    @property
    def k1(self):
        r"""
        Periodic stiffness :math:`k_1`.
        """
        return self._k1

    @k1.setter
    def k1(self, value):
        self._k1 = value
        self.update_dependent_parameters()

    @property
    def damp(self):
        r"""
        Damping :math:`c`.
        """
        return self._c

    @damp.setter
    def damp(self, value):
        self._damp = value
        self.update_dependent_parameters()

    @property
    def omega_A(self):
        r"""
        Frequency of the spring :math:`\omega_A`.
        """
        return self._omega_A

    @omega_A.setter
    def omega_A(self, value):
        self._omega_A = value
        self.update_dependent_parameters()

    # System A(t) matrix.
    def a_mat_fun(self, t):
        r"""
        System :math:`\boldsymbol{A}(t)` matrix.

        Parameters
        ----------
        t : float
            Time :math:`t`.

        Returns
        -------
        ndarray
            System :math:`\boldsymbol{A}(t)` matrix.

        """
        # fmt: off
        return np.array([[0.0,                                                               1.0],
                         [-self._omega_02 - self._omega_12 * np.cos(self.omega_A * t), -self._cc]])
        # fmt: on

    # System B(t) matrix.
    def b_mat_fun(self, t):
        r"""
        System :math:`\boldsymbol{B}(t)` matrix.

        Parameters
        ----------
        t : float
            Time :math:`t`.

        Returns
        -------
        ndarray
            System :math:`\boldsymbol{B}(t)` matrix.

        """
        # fmt: off
        return np.array([[0.0],
                         [1.0 / self._m]])
        # fmt: on

    # System C(t) matrix.
    def c_mat_fun(self, t):
        r"""
        System :math:`\boldsymbol{C}(t)` matrix.

        Parameters
        ----------
        t : float
            Time :math:`t`.

        Returns
        -------
        ndarray
            System :math:`\boldsymbol{C}(t)` matrix.

        """
        return np.array([[1.0, 0.0]])
