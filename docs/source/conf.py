# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import wtsysid

sys.path.insert(0, os.path.abspath('../..'))


# -- Project information -----------------------------------------------------

project = "Wind turbines system identification"
copyright = "2023, DTU Wind and Energy Systems"
author = "DTU Wind and Energy Systems"


# The full version, including alpha/beta/rc tags
release = wtsysid.__version__


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.mathjax",
    "sphinx.ext.doctest",
    "numpydoc",
    # 'sphinx.ext.autosummary',  # Loaded by numpydoc
    "sphinx.ext.napoleon",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
    "myst_nb",
]


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

autodoc_default_flags = [
    "private-members",
]

numpydoc_show_class_members = False 

# autodoc_default_options = {
#     'members': True,
#     'special-members': '__init__',
#     'undoc-members': True,
# }

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "pydata_sphinx_theme"

html_context = {
    "gitlab_repo": "https://gitlab.windenergy.dtu.dk/wtstab/sysid",
}

html_theme_options = {
    "icon_links": [
        {
            # Label for this link
            "name": "GitLab",
            # URL where the link will redirect
            "url": "https://gitlab.windenergy.dtu.dk/wtstab/sysid",  # required
            # Icon class (if "type": "fontawesome"), or path to local image (if "type": "local")
            "icon": "fa-brands fa-square-gitlab",
            # The type of image to be used (see below for details)
            "type": "fontawesome",
        }
   ]
}

myst_enable_extensions = [
    "amsmath",
    # "attrs_inline",
    # "colon_fence",
    # "deflist",
    "dollarmath",
    # "fieldlist",
    # "html_admonition",
    # "html_image",
    # "linkify",
    # "replacements",
    # "smartquotes",
    # "strikethrough",
    # "substitution",
    # "tasklist",
]

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']

