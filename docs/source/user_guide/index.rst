==========
User Guide
==========

The User Guide covers how to identify some analytical model.

Guides
-------

.. If you update this toctree, also update the manual toctree in the
   main index.rst.template

.. toctree::
    :maxdepth: 1

    mathieu_free_stable
    mathieu_forced_constant
    mathieu_forced_noise
    mathieu_free_unstable
