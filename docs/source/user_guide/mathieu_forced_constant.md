---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python
---

# Identification of a Mathieu oscillator when it is forced by a steady input

```{code-cell} python
:tags: [hide-cell]
import matplotlib.pyplot as plt

# Ensure vector graphics.
from IPython.display import set_matplotlib_formats
set_matplotlib_formats("svg")

# Set custom style.
plt.style.use("docs.mplstyle")
```

## Simulation of the forced response

The Mathieu oscillator is a an harmonic oscillator where the stiffness varies periodically in time.

The equation of motion is
    
```{math}
    \begin{pmatrix}
        \dot{x}   \\
        \ddot{x}
    \end{pmatrix} &= \begin{bmatrix}
            0                                        &   1                   \\
        -\omega_0^2 - \omega_1^2 \cos(\Omega t)   &   -2\zeta\omega_0
    \end{bmatrix} \begin{pmatrix}
        x        \\
        \dot{x}
    \end{pmatrix} + \begin{bmatrix}
        0     \\
        1/m
    \end{bmatrix} \bar{u}
    %
    \\
    %
    y &= \begin{bmatrix}
        1   &   0
    \end{bmatrix} \begin{pmatrix}
        x        \\
        \dot{x}
    \end{pmatrix}
```
where $\bar{u}$ is the steady input.

Let's start by simulating its free response

```{code-cell} python
import numpy as np
from wtsysid.examples.mathieu import Mathieu

# Instantiate the Mathieu oscillator with default parameters.
mathieu_c = Mathieu()

# Initial condition.
x0 = np.array([1.0, 0.0])

# Input.
u = np.array([0.3])

# Simulate 100 periods.
time_end = mathieu_c.period * 100  # [s]
n_points_per_period = 400  # Number of points per period.

# Simulation.
time, y = mathieu_c.simulate(time_end, n_points_per_period, x0=x0, u=u)
```

The beginning of the output time series looks like

```{code-cell} python
fig, ax = plt.subplots()
ax.set_xlabel("Time [s]")
ax.set_ylabel("Displacement [m]")
ax.plot(time, y[0, :])
ax.set_xlim(0.0, 70.0)
```

The Nyquist frequency of this time series is

```{code-cell} python
dt = np.mean(np.diff(time))
df = 1 / (time[-1] - time[0])
sampling_frequency = 1 / dt  # [Hz]
nyquist_frequency = sampling_frequency / 2  # [Hz]
print(f"Nyquist frequency of the simulated time history: {nyquist_frequency:.2f} Hz")
```

and allows to compute the Power Spectral Density (PSD).

```{code-cell} python
from scipy import signal

# Parameters.
window = np.ones(len(time) - 1)
nperseg = len(window)
noverlap = 0
nfft = nperseg
detrend = "constant"
return_onesided = True
scaling = "density"

# PSD.
y_frequency, y_PSD = signal.welch(
    y[0, :-1].T,
    sampling_frequency,
    window,
    nperseg,
    noverlap,
    nfft,
    detrend,
    return_onesided,
    scaling,
    axis=0,
)

fig, ax = plt.subplots()
ax.set_xlabel("Frequency [Hz]")
ax.set_ylabel(r"Displacement [$\mathregular{m^2/Hz}$]")
ax.set_yscale("log")
ax.set_xlim(0.0, 0.5)
ax.set_ylim(1e-5, 1e2)
ax.plot(y_frequency, y_PSD)
```
We can see that the PSD does not only contain the free response, but also sharp peaks called $n\mathrm{P}$, as predicted by the harmonic transfer function. Since we have simulated for an integer number of periods, each $n\mathrm{P}$ is contained in exactly 1 frequency bin.

Let us apply Floquet theory to compute its stability analysis.

```{code-cell} python
# Convert the system to discrete time.
mathieu_d = mathieu_c.c2d(n_points_per_period)

# Apply Floquet theory.
mathieu_d.floquet()

# Gather the results.
res_exact = mathieu_d.print_floquet_results(nh=3, file=None, method="sort")

# Display the first mode.
res_exact[0]
```

As we can see, Floquet theory allows to predict all peaks observed in the PSD.

## Data pre-processing for the identification

The Nyquist frequency used for the simulation is much higher than the frequency content of the system. Therefore, we decimate the time history before the identification.

```{code-cell} python
# Downsample the simulated output.
downsample_step = 8

# Digital anti-aliasing filter.
cutoff_frequency = nyquist_frequency / downsample_step  # [Hz]
sos = signal.butter(
    N=6, Wn=1.0 / downsample_step, btype="low", analog=False, output="sos"
)

# Apply the filter.
y_filtered = signal.sosfiltfilt(sos, y)

# Decimate.
time_ident = time[0:-1:downsample_step]
dt_ident = np.mean(np.diff(time_ident))
y_ident = y[:, 0:-1:downsample_step]
period_ident = int(np.round(n_points_per_period / downsample_step))
print(f"Discrete time period: {period_ident}")

# Frequency properties of downsampled output.
df_ident = 1 / (time_ident[-1] - time_ident[0])
sampling_frequency_ident = 1 / dt_ident  # [Hz]
nyquist_frequency_ident = sampling_frequency_ident / 2  # [Hz]
print(f"Nyquist frequency of the decimated time history: {nyquist_frequency_ident:.2f} Hz")

# Compute PSD.
window_ident = np.ones(len(time_ident) - 1)
nperseg_ident = len(window_ident)
nfft_ident = nperseg_ident

y_frequency_ident, y_ident_PSD = signal.welch(
    y_ident[0, :-1].T,
    sampling_frequency_ident,
    window_ident,
    nperseg_ident,
    noverlap,
    nfft_ident,
    detrend,
    return_onesided,
    scaling,
    axis=0,
)

# Plot PSD.
fig, ax = plt.subplots()
ax.set_xlabel("Frequency [Hz]")
ax.set_ylabel(r"Displacement [$\mathregular{m^2/Hz}$]")
ax.set_yscale("log")
ax.set_xlim(0.0, 0.5)
ax.set_ylim(1e-5, 1e2)
ax.plot(y_frequency, y_PSD, label="Simulated")
ax.plot(y_frequency_ident, y_ident_PSD, label="Decimated")
ax.legend()
```

As we can see, the decimation process has not altered the system modes, but has substantially reduced the Nyquist frequency.

We are now ready to identify this system.

## Identification with the Periodic MOESP

Let's call the Periodic MOESP with 2 states and a past window of 40 time steps.
```{code-cell} python
from wtsysid.identification import pmoesp

# Create the input.
u_ident = np.broadcast_to(u, (1, time_ident.size))

# Identify.
sys_ident_pmoesp, x0_ident_pmoesp, singular_values_pmoesp = pmoesp(
    y_ident, nx=2, period=period_ident, past_window=40,
    u=u_ident, zero_mean_input=False,
)
sys_ident_pmoesp.dt = dt_ident
```
The singular values reveal that this system has indeed 2 states
```{code-cell} python
fig, ax = plt.subplots()
ax.set_xlabel("Number [-]")
ax.set_ylabel("Singular values")
ax.set_yscale("log")
for t in range(singular_values_pmoesp.shape[0]):
    ax.scatter(np.full((singular_values_pmoesp.shape[1]), t), singular_values_pmoesp[t, :])
```

Simulating the identified system provides
```{code-cell} python
# Simulate.
y_predicted_pmoesp = sys_ident_pmoesp.simulate(time_ident.size,
                                               x0=x0_ident_pmoesp,
                                               u=u_ident)

# PSD.
y_frequency_pmoesp, y_predicted_pmoesp_PSD = signal.welch(
    y_predicted_pmoesp[0, :-1].T,
    sampling_frequency_ident,
    window_ident,
    nperseg_ident,
    noverlap,
    nfft_ident,
    detrend,
    return_onesided,
    scaling,
    axis=0,
)

# Plot PSD.
fig, ax = plt.subplots()
ax.set_xlabel("Frequency [Hz]")
ax.set_ylabel(r"Displacement [$\mathregular{m^2/Hz}$]")
ax.set_yscale("log")
ax.set_xlim(0.0, 0.5)
ax.set_ylim(1e-5, 1e2)
ax.plot(y_frequency, y_PSD, label="Simulated")
ax.plot(y_frequency_pmoesp, y_predicted_pmoesp_PSD, label="Predicted")
ax.legend()
```
As we can see, the identified PSD has an offset compared to the simulated one. This is because a steady input is not persistently exciting, and therefore the Periodic MOESP cannot exactly identify the $\boldsymbol{B}(t)$ and $\boldsymbol{D}(t)$ matrices. However, the $\boldsymbol{A}(t)$ and $\boldsymbol{C}(t)$ matrices are correct, and therfore the stability analysis matches the exact result.
```{code-cell} python
# Floquet stability analysis of the identified systems.
sys_ident_pmoesp.floquet()

# Gather the results.
res_pmoesp = sys_ident_pmoesp.print_floquet_results(nh=3, file=None, method="sort")

# Display the first mode.
res_pmoesp[0]
```

## Identification with the Partial Floquet Analysis

Let's call the Partial Floquet Analysis (PFA) with 3 states and a past window of 40 time steps.
```{code-cell} python
from wtsysid.identification import pfa

# Identify.
natural_frequencies_pfa, damping_ratios_pfa, singular_values_pfa, mode_shapes_pfa = pfa(
    y_ident,
    nx=3,
    period=period_ident,
    past_window=40,
    dt=dt_ident,
    compute_mode_shapes=True,
    )
```

The PFA assumes that the system is autonomous, and therefore identifies the $n\mathrm{P}$ as modes. This is qualitatively wrong, but in this case we are mostly interested in the stability properties.
```{code-cell} python
fig, ax = plt.subplots()
ax.set_xlabel("Number [-]")
ax.set_ylabel("Singular values")
ax.set_yscale("log")
ax.scatter(np.arange(singular_values_pfa.size), singular_values_pfa)
```

We can now look for the principal harmonic.
```{code-cell} python
# Compute mode shapes magnitude.
# There is only 1 output channel, so we can discard the last dimension.
mode_shapes_pfa_mag = np.abs(mode_shapes_pfa[:, :, 0])

# Compute output-specific participation factors.
participation_pfa = mode_shapes_pfa_mag / np.sum(mode_shapes_pfa_mag, axis=0)

# The principal harmonic is the one with the maximum participation.
i_principal_harmonic = np.argmax(participation_pfa, axis=0)
```

Let's take some harmonics below and above the principal one.
```{code-cell} python
# Number of harmonics to include.
nh = 3

# Indices of the harmonics. Axes ordered as:
#   - 0: harmonics.
#   - 1: modes.
i_harmonics = i_principal_harmonic[np.newaxis, :] + np.arange(-nh, +nh+1)[:, np.newaxis]

# Modes indices. Broadcasted over the harmonics.
nx = natural_frequencies_pfa.shape[1]
ix = np.arange(nx)[np.newaxis, :]

# Reduced set of results.
natural_frequencies_pfa_red = natural_frequencies_pfa[i_harmonics, ix]
damping_ratios_pfa_red = damping_ratios_pfa[i_harmonics, ix]
participation_pfa_red = participation_pfa[i_harmonics, ix]

# Finally, collect the results into some DataFrame.
import pandas as pd

# Collect the first mode into a DataFrame.
res_pfa = []
for i in range(nx):
    res_pfa.append(
        pd.DataFrame(
            data=np.column_stack((natural_frequencies_pfa_red[:, i],
                                  damping_ratios_pfa_red[:, i] * 100.0,
                                  participation_pfa_red[:, i])),
            columns=("Natural frequency [Hz]",
                     "Damping ratio [%]",
                     "Participation [-]",
                     ),
            index=pd.Index(np.arange(-nh, +nh+1), name="Harmonic"),
        )
    )
```
The first mode contains the $n\mathrm{P}$. Therefore, the frequencies are $n\Omega$, with $n \in \mathbb{N}$, while the damping ratios are all 0. The other 2 modes are the physical ones and match the analytical result.
```{code-cell} python
res_pfa[1]
```
