*********************
WTSysId documentation
*********************

The **WTSysId** package provides system identification algorithms for the stability analysis of operating wind turbines.

============
Installation
============

========
Contents
========

.. toctree::
    :maxdepth: 3
    :titlesonly:

    user_guide/index
    api_reference/index